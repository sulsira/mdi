
 	var realestate = {
 		init: function(config){
 			this.config = config;
 			this.trigger = config['trigger'];
 			this.submited = config['form'];
 			this.work = config['work'];
 			// this.template = config['template'];
 			// get the trigger and get the data
 			this.setupTemplate();
 			this.eventsBind.click.call(this);
 			this.eventsBind.submit.call(this);

 		},
 		eventsBind: {

 			click: function(){
 				var self = this; // refers to the globla object
 				self.trigger.on('click',self.shiver);
 				$('.add-more-right').on('click',function(e){
 					console.log(this)
 					e.preventDefault();
 				});
 			},
 			submit: function(){
 				var self = this; // refers to the globla object
 				self.submited.on('submit',self.processTenant);
 				// self.work.on('submit',self.processTenant);
 			}
 		},
 		shiver: function(e){
 			var self = realestate;
 			e.preventDefault();
 			var type = $(this).data('type');
 			var content = $(this).data('content'); 
 			var container = $(this).data('container');
 			// if (type === 'add-school') self.addSchool(container,content,type);
 		},
 		addSchool: function(container,content,type){
 			var self = this;
 			var table = $('.'+container).first('table.table');
 			var tbody = table.find('tbody');
 			var tr = tbody.find('tr.'+content);
 			// var	tr = table.find('tr.'+content);
 			console.log($(tr).filter('td'))
 		},
 		setupTemplate: function() {
 			this.config.template = Handlebars.compile(this.config.template);
 			// this.config.workTemplate = Handlebars.compile(this.config.workTemplate);
 			// 
 		},
 		processSchool: function(e){
 			var self = realestate;
 			var url = $(this).prop('action');
 			var	table = self.submited.find('table').eq(0);
 			var tbody =	table.find('tbody').eq(0);
 			$.ajax({
 				url: url,
 				data: $(this).serialize(),
 				type: 'POST',
 				dataType: 'JSON',
 				success: function(data){
 				var hello = 
 					tbody
 						.append( function(){
 							$(this).append(self.config.template(data));
 							return this;
 						} );
 						console.log(data);
					$(hello).find('tr.new-school').find('a.trigger').on( 'click', self.newSchool );

 				},
 				error: function(error,dd){
 					console.log(error);
 					console.log('errored');
 				}
 			});
 			e.preventDefault();
 		},
 	newSchool: function(e){
 		var $this = $(this);
 		var cont = $(this).parent().find('div.add-subject').slideDown(200);
 		$(cont).find('form').on('submit', function(e){
 			var url = $(this).prop('action');
 			$.ajax({
 				data: $(this).serialize() ,
 				url: url,
 				type: 'POST',
 				dataType: 'JSON',
 				success: function(results){
 					if (results === true) {
 						cont.slideUp(200);
 						$this.detach();
 					}else{
 						cont.slideUp(200);
 						$this.parents('tr.new-school').eq(0).addClass('alert alert-error')
 					}
 				},
 				error: function(error, message){
 					console.log(message);
 				}

 			});
 			e.preventDefault();
 		});
 		e.preventDefault();
 	},
 	processTenant: function(e){
 		var self = realestate;
 		var url = $(this).prop('action');
 		 	var	table = self.submited.find('table').eq(0);
 			var tbody =	table.find('tbody').eq(0);
 			console.log(tbody);
 			$.ajax({
 				url: url,
 				data: $(this).serialize(),
 				type: 'POST',
 				dataType: 'JSON',
 				success: function(data){
 				var hello = 
 					tbody
 						.append( function(){
 							$(this).append(self.config.template(data));
 							return this;
 						} );
 						console.log(data);
					// $(hello).find('tr.new-school').find('a.trigger').on( 'click', self.newSchool );

 				},
 				error: function(error,dd){
 					console.log(error);
 					console.log('errored');
 				}
 			});
 		e.preventDefault();
 	}


 	} // end of process school

realestate.init({
	trigger: $('.trigger'),
	form: $('.tenant'),
	template: $('#tenants').html(),
	workTemplate: $('#new-work').html(),
	container: $('.pre-container'),
	work: $('#work')
 	});

