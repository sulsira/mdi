<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_roles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->default(0);
			$table->string('type')->nullable();  
			$table->string('fullname')->nullable();
			$table->string('ip')->default(0);
			$table->string('pc')->nullable();	
			$table->string('privileges')->nullable(); // veda(view edit delete add)
			$table->string('department_id')->nullable(); // eg school admin records guest: 
			$table->string('userGroup')->nullable();  // moherst school guest
			$table->boolean('link')->default(0);	// yes or not : is a school or not
			$table->integer('lickId')->default(0); // school id
			$table->boolean('deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_roles');
	}

}
