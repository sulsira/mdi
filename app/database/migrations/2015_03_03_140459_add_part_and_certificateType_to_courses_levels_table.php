<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPartAndCertificateTypeToCoursesLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('courses_levels', function(Blueprint $table)
		{
			$table->string('certificate_type')->nullable()->after('duration');
			$table->integer('part')->default(0)->after('certificate_type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('courses_levels', function(Blueprint $table)
		{
			
		});
	}

}
