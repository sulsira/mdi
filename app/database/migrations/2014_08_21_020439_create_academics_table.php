<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAcademicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('academics', function(Blueprint $table)
		{
			$table->increments('aca_id');
			$table->integer('aca_studentID')->default(0);
			$table->string('aca_schoolname')->nullable();
			$table->date('aca_datefrom')->nullable();
			$table->date('aca_dateto')->nullable();
			$table->string('aca_subject')->nullable();
			$table->string('aca_grade')->nullable();
			$table->string('aca_schoolType')->nullable();
			$table->string('aca_uppersecondaryFrom')->nullable();
			$table->integer('deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('academics');
	}

}
