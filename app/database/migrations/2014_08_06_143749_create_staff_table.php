<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('Staff_PersonID')->default(0);
			$table->string('Staff_Rank')->nullable();
			$table->string('Staff_HighestQualification');
			$table->string('Staff_Role');
			$table->string('Staff_FieldOfTeaching');
			$table->string('Staff_MainLevelTeaching');
			$table->string('Staff_HireDate')->nullable();
			$table->string('Staff_EmploymentType')->nullable();
			$table->integer('dept_id')->default(0);
			$table->boolean('deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('staff');
	}

}
