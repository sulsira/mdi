<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCertificatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('certificates', function(Blueprint $table)
		{
			$table->increments('cert_id');
			$table->integer('cert_studentID')->default(0);
			$table->integer('cert_academicID')->default(0);
			$table->string('cert_schoolName')->nullable();
			$table->date('cert_from')->nullable();
			$table->date('cert_to')->nullable();
			$table->string('cert_subject')->nullable();
			$table->string('cert_grades')->nullable();
			$table->integer('deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('certificates');
	}

}
