<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Cont_ContactInfoID')->nullable();
			$table->string('Cont_EntityID')->nullable();
			$table->string('Cont_EntityType')->nullable();
			$table->string('Cont_Contact')->nullable();
			$table->string('Cont_ContactType')->nullable();
			$table->boolean('Cont_Deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
