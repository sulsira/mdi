<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Applicants', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('std_id')->default(0)->index();
			$table->integer('person_id')->default(0)->index();
			$table->enum('application_person',['applicant', 'student']);
			$table->string('form_serialNumber')->nullable();
			$table->string('student_type')->nullable();
			$table->string('school_payment')->nullable();
			$table->string('employment_status')->nullable();
			$table->string('upper_secondary')->nullable();
			$table->string('job_type')->nullable();
			$table->string('interviewed_on')->nullable();
			$table->string('consent')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Applicants');
	}

}
