<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses_levels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('course_id')->default(0);
			$table->string('name')->nullable();
			$table->string('fee')->nullable();
			$table->string('duration')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses_levels');
	}

}
