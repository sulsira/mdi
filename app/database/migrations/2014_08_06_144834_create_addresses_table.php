<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function(Blueprint $table)
		{
			$table->increments('Addr_AddressID');
			$table->string('Addr_EntityID')->nullable();
			$table->string('Addr_EntityType')->nullable();
			$table->string('Addr_AddressStreet')->nullable();
			$table->string('Addr_POBox')->nullable();
			$table->string('Addr_Town')->nullable();
			$table->string('Addr_Region')->nullable();
			$table->string('Addr_District')->nullable();
			$table->string('Addr_Country')->nullable();
			$table->string('Addr_CareOf')->nullable();
			$table->boolean('Addr_Deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('addresses');
	}

}
