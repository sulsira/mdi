<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Documents', function(Blueprint $table)
		{
			$table->increments('doc_id');
			$table->string('doc_title')->nullable();
			$table->string('doc_entityType')->nullable();
			$table->string('doc_entityID')->default(0);
			$table->string('doc_type')->nullable();
			$table->string('doc_fullpath')->nullable();
			$table->string('doc_filename')->nullable();
			$table->string('doc_foldername')->nullable();
			$table->string('doc_extension')->nullable();
			$table->string('doc_filetype')->nullable();
			$table->string('doc_userID')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Documents');
	}

}
