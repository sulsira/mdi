<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmploymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employments', function(Blueprint $table)
		{
			$table->increments('emp_id');
			$table->integer('emp_studentID')->default(0);
			$table->string('emp_type')->nullable();
			$table->string('emp_employer')->nullable();
			$table->string('emp_from')->nullable();
			$table->string('emp_to')->nullable();
			$table->string('emp_positionHeld')->nullable();
			$table->string('emp_duties')->nullable();
			$table->integer('deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employments');
	}

}
