<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->increments('pay_id');
			$table->integer('pay_studentID')->default(0);
			$table->integer('pay_courseID')->default(0);
			$table->string('pay_payer')->nullable();
			$table->string('pay_totalAssesment')->nullable();
			$table->string('pay_initialPayment')->nullable();
			$table->string('pay_amountPaid')->nullable();
			$table->string('pay_orNumber')->nullable();
			$table->string('pay_receivedBy')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
