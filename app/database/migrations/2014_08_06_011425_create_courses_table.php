<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('depart_id')->default(0);
			$table->string('name')->nullable();
			$table->string('fee')->nullable();
			$table->string('entry_qualification')->nullable();  // 
			$table->string('qualification')->nullable();
			$table->string('duration')->nullable(0);
			$table->integer('visibility')->default(0); // school id
			$table->boolean('deleted')->default(0);

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}
