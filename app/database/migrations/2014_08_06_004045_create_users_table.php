<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->nullable();
			$table->string('email')->nullable();
			$table->string('hpwd')->nullable();
			$table->boolean('link')->default(0);	// yes or not : is a school or not
			$table->integer('lickId')->default(0); // school id
			$table->integer('visible')->default(0);
			$table->integer('deleted')->default(0);
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
