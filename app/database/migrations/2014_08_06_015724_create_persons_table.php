<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('pers_indentifier')->nullable();
			$table->integer('pers_givenID')->default(0);
			$table->string('pers_fname')->nullable();
			$table->string('pers_mname')->nullable();
			$table->string('pers_lname')->nullable();
			$table->string('pers_type')->nullable();
			$table->date('pers_DOB')->nullable();
			$table->string('pers_gender')->nullable();
			$table->string('pers_nationality')->nullable();
			$table->string('pers_ethnicity')->nullable();
			$table->string('pers_standing')->nullable();
			$table->string('pers_NIN')->nullable();
			$table->boolean('pers_deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons');
	}

}
