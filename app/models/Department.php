<?php

class Department extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = ['id','name', 'person_id','personName', 'link', 'lickId', 'deleted'];


	public function courses(){
		return $this->hasMany('Course','depart_id');
	}
	public function staffs(){
		return $this->hasMany('Staff','staff_id');
	}
}