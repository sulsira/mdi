<?php

class Document extends \Eloquent {

	protected $table = 'documents';
	protected $primaryKey = 'doc_id';
	protected $fillable = [
			'doc_id',
			'doc_title',
			'doc_entityType',
			'doc_entityID',
			'doc_type',
			'doc_fullpath',
			'doc_filename',
			'doc_foldername',
			'doc_extension',
			'doc_filetype',
			'doc_userID'
	];

	public function person(){
		return $this->belongsTo('Person', 'doc_id', 'id' );
	}
}