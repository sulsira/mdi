<?php

class Academic extends \Eloquent {
	protected $fillable = [
		'aca_id',
		'aca_studentID',
		'aca_schoolname',
		'aca_datefrom',
		'aca_dateto',
		'aca_subject',
		'aca_grade',
		'aca_schoolType',
		'aca_uppersecondaryFrom',
		'deleted'
	];

	public function student(){
		return $this->belongsTo('Student','aca_studentID', 'id');
	}
	public function grades(){
		return $this->hasMany('Grade','ace_recordID','aca_id');
	}
	public function certificates(){
		return $this->hasMany('Certificate','cert_academicID','aca_id');
	}
}