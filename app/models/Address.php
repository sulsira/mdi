<?php

class Address extends \Eloquent {
	protected $fillable = [
	'Addr_AddressID',
	'Addr_EntityID',
	'Addr_EntityType',
	'Addr_AddressStreet',
	'Addr_POBox',
	'Addr_Town',
	'Addr_Region',
	'Addr_District',
	'Addr_Country',
	'Addr_CareOf',
	'Addr_Deleted'
	];
}