<?php

class Contact extends \Eloquent {
	protected $fillable = ['Cont_EntityID',
'Cont_EntityType',
'Cont_Contact',
'Cont_ContactType'];


	public function persons(){
		return $this->belongsTo('Person','Cont_EntityID','id');
	}
	public function students(){
		return $this->belongsTo('Student');
	}
	public function staffs(){
		
	}

}