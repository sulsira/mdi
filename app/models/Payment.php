<?php

class Payment extends \Eloquent {
	protected $fillable = [
'pay_id',
'pay_studentID',
'pay_courseID',
'pay_payer',
'pay_totalAssesment',
'pay_initialPayment',
'pay_amountPaid',
'pay_orNumber',
'pay_receivedBy'
	];
}