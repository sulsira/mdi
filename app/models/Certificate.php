<?php

class Certificate extends \Eloquent {
	protected $fillable = [
			'cert_id',
			'cert_studentID',
			'cert_academicID',
			'cert_schoolName',
			'cert_from',
			'cert_to',
			'cert_subject',
			'cert_grades',
			'deleted',
			'url',
			'folder'
	];

	public function academic(){
		return $this->belongsTo('Academic', 'ace_id','cert_academicID');
	}
}