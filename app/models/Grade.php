<?php

class Grade extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = ['id','ace_recordID'];

	public function academic(){
		return $this->belongsTo('Academic','ace_recordID','ace_id');
	}
}