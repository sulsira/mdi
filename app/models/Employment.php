<?php

class Employment extends \Eloquent {
	protected $fillable = [
'emp_id',
'emp_studentID',
'emp_type',
'emp_employer',
'emp_from',
'emp_to',
'emp_positionHeld',
'emp_duties',
'deleted'
	];
	public function student(){
		return $this->belongsTo('Studend','emp_studentID','id');
	}
}