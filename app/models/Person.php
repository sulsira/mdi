<?php

class Person extends \Eloquent {
	public $table = 'persons';
	protected $fillable = [
	'pers_indentifier', 
	'pers_givenID', 
	'pers_fname', 
	'ers_mname', 
	'pers_lname', 
	'pers_type', 
	'pers_DOB', 
	'pers_gender', 
	'pers_nationality', 
	'pers_ethnicity', 
	'pers_standing', 
	'pers_NIN',
	'pers_deleted'  
	];

	public function scopeLecturers($query){
		$staff_set = array();
		$staff = $query->where('pers_type','=','Staff')->join('Staff',function($join){
			$join->on('persons.id','=', 'Staff.Staff_PersonID')->where('Staff.Staff_Role','!=', 'Administrative');
		})->get()->toArray();

		if ($staff) {
			foreach ($staff as $key => $value) {
				$staff_set[] = $value;
				// $staff_set['staff'] = $value;
				// if (!empty($value['dept_id'])) {
				// 	var_dump($value['dept_id']);
				// 	$dept =  Department::where('id', '=',$value['dept_id']);
				// 	if (!empty($dept)) {
				// 		$staff_set['staff']['department'] = $dept->get()->toArray();
				// 	}
					
				// }
			}
		}
		return $staff_set;
	}
	public function scopeStudent($query,$type){
		return $query->whereRaw('id = ? AND pers_type = ?',[$type,'Student'])->get();
	}
	public function students(){
		return $this->belongsTo('Student','id','person_id');
	}
	public function staffs(){
		return $this->hasOne('Staff','Staff_PersonID','id');
	}
	public function scopeEntity($query,$type){
		return $query->whereRaw('id = ? AND pers_type = ?',[$type])->get();
	}
	public function contacts(){
		return $this->hasOne('Contact','Cont_EntityID');
	}
	public function document(){
		return $this->hasOne('Document', 'doc_entityID', 'id');
	}
}