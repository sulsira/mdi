<?php

class Guardian extends \Eloquent {
	protected $primaryKey = 'id';

	protected $fillable = ['fullname','contact','applicant_id','student_id'];

	public function student(){
		return $this->belongsTo('Student','id','student_id');
	}
}