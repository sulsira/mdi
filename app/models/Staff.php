<?php

class Staff extends \Eloquent {
	protected $primaryKey = 'staff_id';
	protected $table = 'staff';
	protected $fillable = [
	'staff_id'
	,'Staff_PersonID'
	,'Staff_Rank'
	,'Staff_HighestQualification'
	,'Staff_Role'
	,'Staff_FieldOfTeaching'
	,'Staff_MainLevelTeaching'
	,'Staff_HireDate'
	,'Staff_EmploymentType'
	,'dept_id'
	,'deleted'
	,'staffID'
];
	 public function scopeLecturer($query,$type){

	 	return  $query->where('Staff_id','=',$type)->join('persons', function($join){
	 		$join->on('persons.id','=', 'staff.Staff_PersonID')->where('persons.pers_deleted','=', 0);
	 	})->first();


	 }

	public function courses(){
		return $this->hasMany('Course','staffID');
	}

	public function persons(){
		return $this->hasOne('Person','id','Staff_PersonID');
	}
	public function departments(){
		return $this->belongsTo('Department','dept_id','id');
	}
}