<?php

class Student extends \Eloquent {
	protected $fillable = [
			'id',
			'person_id',
			'admission_date',
			'course_id',
			'graduated',
			'completion_date',
			'course_level_id',
			'studentID'
	];

	public function course(){
		return $this->belongsTo('Course', 'course_id','id');
	}
	public function persons(){
		return $this->hasOne('Person','id','person_id');
	}
	public function contacts(){
		return $this->hasMany('Contact','id','person_id');
	}
	public function addresses(){
		return $this->hasOne('Person','id','person_id');
	}
	public function lecturers(){
		return $this->hasOne('Person','id','person_id');
	}
		public function guardian(){
		return $this->hasOne('Guardian','student_id','id');
	}
	public function academics(){
		return $this->hasMany('Academic','aca_id','id');
	}
	public function employments(){
		return $this->hasMany('Employment','emp_id','id');
	}
}