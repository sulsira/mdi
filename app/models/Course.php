<?php

class Course extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = [
				'id',
				'depart_id',
				'name',
				'fee',
				'entry_qualification',
				'qualification',
				'duration',
				'visibility',
				'deleted',
				'staffID'

	];
	public function department(){
		return $this->belongsTo('Department','depart_id','id');
	}
	public function students(){
		return $this->hasMany('Student','course_id','id');
	}
	public function Staffs(){
		return $this->belongsTo('Staff','staffID','staff_id');
	}
	public function scopeGetdepartment($query,$type){

	}
}