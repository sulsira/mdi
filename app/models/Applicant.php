<?php

class Applicant extends \Eloquent {
	protected $fillable = ['std_id', 'person_id', 'application_person', 'form_serialNumber', 'student_type', 'school_payment', 'employment_status', 'upper_secondary', 'job_type', 'interviewed_on', 'consent'];
}