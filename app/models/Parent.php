<?php

class Parent extends \Eloquent {

	protected $table = 'guardians';
	protected $primaryKey = 'id';

	protected $fillable = ['fullname','contact','applicant_id','student_id'];



}