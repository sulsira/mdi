<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
// Route::get('/', 'HomeController@showWelcome');


Route::get('login',array(
		'as' => 'login_page',
		'uses' => 'SessionsController@create'
	));
Route::post('login',array(
		'as' => 'login_path',
		'uses' => 'SessionsController@store'
	));
Route::get('logout','SessionsController@destroy');
Route::controller('page','PagesController');
Route::get('download',function(){
	$file = $_GET['file'];
	return Response::download($file);
});
Route::get('view', function()
{
	$file = $_GET['file'];
	return Redirect::to($file);
});

Route::resource('/','WelcomeController');
// Route::resource('admin','AdminController');
// Route::resource('users','UsersController');

Route::controller('page','PagesController');




// GROUP ROUTES

Route::group(array('before'=>'auth'), function(){

		Route::resource('users','UsersController');
		Route::resource('admin','AdminController');
		Route::resource('admin.staffs','StaffsController');
		Route::resource('departments','DepartmentsController');
		Route::resource('staffs','StaffsController');
		Route::resource('departments.staffs','StaffsController');
		Route::resource('courses','CoursesController');
		Route::resource('departments.courses','CoursesController');
		Route::resource('students','StudentsController');
		Route::resource('students.complete','PreController',array('only'=>array('index','show','store')));
		Route::resource('courses.students','StudentsController');
		Route::resource('search','SearchController',array('only' => array('index','show','store')));
		Route::resource('search.persons','SearchController');
		Route::resource('Documents', 'DocumentsController');
		Route::resource('data_entry', 'DataEntriesController');
		Route::resource('certificates', 'CertificatesController');
		Route::resource('work', 'EmploymentsController');


});
// Route::get('test',function(){
// 	// var_dump(User::select(DB::raw('select * from users')));
	
// });
// Event::listen('illuminate.query',function($query){
// 	// dd($query);
// 	echo '<pre style="margin:200px 80px">';
// 	print_r($query);
// 	echo '</pre>';
// });
// Route::get('/company-profile', function() {
//     return View::make('companyprofile');
// });
