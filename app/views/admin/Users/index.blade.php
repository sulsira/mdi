<?php #page specific processing
 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-user')
<div class="c-header cc">
		<h3>Users</h3>
		<hr>
	<div class="cht">
		<p class="school_details_sumarry">
			<strong><a  href="#add-user" role="button" data-toggle="modal"></i>Add a user</a></strong> 
<!-- 			<strong><a href="">DELETE</a></strong>
			<strong><a href="">Log out</a></strong>
			<strong><a href="">DISABLE</a></strong> -->
		</p>
  		<div class="messages">

			@include('__partials/errors')
		</div>
	</div>
</div>

<div class="cc">
 <section>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Email</th>
				<!-- <th>Edit</th> -->
				<!-- <th>User group</th> -->
				<!-- <th>Status</th> -->
				<!-- <th>Log</th> -->
				<th>create</th>
				<th>action</th>
			</tr>
		</thead>
		<tbody>	
			<?php if (!empty($users)): ?>
				<?php foreach ($users as $key => $value): ?>
					<tr>
						<td>{{$value['email']}}</td>
						
						<!-- <td>-</td> -->
						<td>{{$value['created_at']}}</td>
						<td>
							<div>	
								{{Form::delete('users/'. $value['id'], 'Delete')}}
								<!-- <input	type="checkbox" name="user[]"  data-input=""/>						 -->
							</div>
						</td>
					</tr>
				<?php endforeach ?>
			<?php else: ?>
			<tr>
				<td colspan="6">We could not find any users.</td>	
			</tr>				
			<?php endif ?>

		</tbody>
	</table>
</section>
</div><!-- content conent -->
@stop
@include('templates/bottom-admin')