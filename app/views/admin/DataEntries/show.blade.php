<?php #page specific processing

# global datasets:

$fullname = '';
$studentID = 0;

	if(!empty($student) && isset($student['persons'])):

		$fullname = ucwords($student['persons']['pers_fname']. ' ' .$student['persons']['pers_mname']. ' ' .$student['persons']['pers_lname']);
		$studentID = $student['id'];

	else:


		// echo "suck it";


	endif;


?>
@include('templates/top-admin')
@section('content')
<div class="cc">
		<div class="pre-container">
			<div class="pre-header">
				<h2>Adding new Applicant</h2>
				<p>In this section you add student bio data and status</p>
			</div>
			<hr>
			<div class="pre-content">
				<div class="pch">
					<ul class="steps">
						<li <?php echo ( ( !isset($_GET['step']) ))? "class='active'" : ''; ?> data-stepID="applicant">
							<span class="pre-icon mega-octicon octicon octicon-file-media"></span>
							<strong class="step"><a href="#">Applicant</a></strong>
							add: schools attended an certificates
						</li>
						<li <?php echo ( isset($_GET['step']) && $_GET['step'] == 2 )? "class='active'" : '' ?> data-stepID="work">
							<span class="pre-icon mega-octicon octicon octicon-book"></span>
							<strong class="step"><a href="">Work</a></strong>
							Add: work places, positions held, duration
						</li>
					<li <?php echo ( isset($_GET['step']) && $_GET['step'] == 3 )? "class='active'" : '' ?> data-stepID="student">
							<span class="pre-icon mega-octicon octicon octicon-briefcase"></span>
							<strong class="step"><a href="">Student</a></strong>
							Add: intervied on, course payment
						</li>
					</ul>
				</div>
				<div class="forms">
					<div class="add-academics spacer" id="applicant">
					<h3>{{$fullname}}</h3>	
					{{Form::open(array('route'=>'data_entry.store','files'=>true,'class'=>'tenant'))}}{{Form::hidden('type','aschool')}}{{Form::hidden('studentID',$studentID)}}
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th colspan="2"  rowspan = "1"><a href="" id="sname" class="trigger" data-type="add-School" data-content="forming" data-container="add-academics"><span class="mega-octicon octicon-plus"></span></a>Name of School / College / University</th>
								<th colspan="3">Dates</th>
							</tr>
							<tr>
								<th  colspan="2"></th>
								<th>From</th>
								<th>To</th>
							</tr>
						</thead>
						<tbody>			
							<tr class="forming">


							


										<td colspan="2">

											{{Form::text('SchoolName',null,['placeholder'=>'Add School ','required'=>1,'class'=>"school-name",'autofocus'])}}

										</td>

										<td>
											{{Form::text('from',null,['placeholder'=>'date','required'=>1])}}
										</td>
				
										<td class="last">
											{{Form::text('to',null,['placeholder'=>'date','required'=>1])}}
										<button type="submit" class="add-mit-right" title="add a School you have attended" data-type="aschool"><span class="mega-octicon octicon-plus"></span></button>
										</td>


							</tr>
						</tbody>
					</table>
					{{Form::close()}}
					<div class="add-doc">
						{{Form::open(array('route'=>'certificates.store','files'=>true,'id'=>'uploaddoc'))}}
						{{Form::hidden('type','fileUpload')}}
							<input type="hidden" name="id" id="doc" value="">
							<span class="octicon octicon-triangle-up"></span>
							<hr>
							{{Form::file('doc',null,['placeholder'=>'Add Docuemnt'])}}
							 <span class="help-block">Only pdf, img, docs.</span>
							<hr>
							<input type="submit" class="btn btn-info" name="upload" value="upload a document"></input>
							<input type="submit" class="btn btn-success" name="done" value="upload and done"></input>
						{{Form::close()}}
					</div>
					<div class="add-subject">
						<span class="octicon octicon-triangle-up"></span>
						<hr>
						<h3>certificate name</h3>
						<hr>
						<div class="subgrad">
						{{Form::open(array('route'=>'data_entry.store','id'=>'grad'))}}
						{{Form::hidden('type','grades')}}
						<input type="hidden" name="id" id="id" value="">
							<table>
								<thead>
									<tr>
										<th>Subjects</th>
										<th>Grade</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td rowspan="2">
											<button type="submit" class="btn btn-info">Save subjects</button>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<tr>
										<td><input type="text" name="one[sub]"></td>
										<td><input type="text" name="one[grd]"></td>
									</tr>
									<tr>
										<td><input type="text" name="two[sub]"></td>
										<td><input type="text" name="two[grd]"></td>
									</tr>
									<tr>
										<td><input type="text" name="three[sub]"></td>
										<td><input type="text" name="three[grd]"></td>
									</tr>
									<tr>
										<td><input type="text" name="four[sub]"></td>
										<td><input type="text" name="four[grd]"></td>
									</tr>
									<tr>
										<td><input type="text" name="five[sub]"></td>
										<td><input type="text" name="five[grd]"></td>
									</tr>
									<tr>
										<td><input type="text" name="six[sub]"></td>
										<td><input type="text" name="six[grd]"></td>
									</tr>
									<tr>
										<td><input type="text" name="seven[sub]"></td>
										<td><input type="text" name="seven[grd]"></td>
									</tr>
								</tbody>
							</table>
							{{Form::close()}}
						</div>
					</div>
					</div> <!-- end of academics -->
					<div class="add-academics spacer work" style="display:none" id="work">
					<h3>Work</h3>	
					{{Form::open(array('route'=>'work.store','id'=>'work','class'=>'tenant'))}}
					{{Form::hidden('type','work')}}
					{{Form::hidden('studentID',$studentID)}}
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th colspan="1"  rowspan = "1"><a href="" id="sname" class="trigger" data-type="add-School" data-content="forming" data-container="add-academics"><span class="mega-octicon octicon-plus"></span></a>Name and address of employee</th>
								<th colspan="3">Dates</th>
								<th>Position held</th>
								<th>Duties</th>
							</tr>
							<tr>
								<th  colspan="2"></th>
								<th>From</th>
								<th>To</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>			
							<tr class="forming">


							


										<td colspan="2">

											{{Form::text('employer',null,['placeholder'=>'Employer ','required'=>1,'class'=>"school-name",'autofocus'])}}

										</td>

										<td>
											{{Form::text('from',null,['placeholder'=>'date','required'=>1])}}
										</td>
														<td>
											{{Form::text('to',null,['placeholder'=>'date','required'=>1])}}
										</td>
																				<td>
											{{Form::text('position',null,['placeholder'=>'Enter position held','required'=>1])}}
										</td>
										<td class="last">
											{{Form::text('duties',null,['placeholder'=>'Enter Duties','required'=>1])}}
										<button type="submit" class="add-mit-right" title="add a School you have attended" data-type="aschool"><span class="mega-octicon octicon-plus"></span></button>
										</td>


							</tr>
						</tbody>
					</table>
					{{Form::close()}}
					</div> <!-- end of work -->
					<div class="add-academics spacer credentials" id="student">
					<h3>Credentials</h3>	
					{{Form::open(array('route'=>['students.update',$studentID], 'method'=>'PUT'))}}
					{{Form::hidden('type','studentID')}}
						<div class="input-prepend input-append">
						  <span class="add-on">Student Number/ID</span>
						  <input class="span2" id="appendedPrependedInput" type="text" name="student_number">
						  <span class="add-on">#</span>
						</div>
						<div class="form-actions">
						  <button type="submit" class="btn btn-info" name="create_studentCode">Create</button>
						  <button type="button" class="btn btn-success">Generate</button>
						</div>
					{{Form::close()}}
					</div> <!-- end of credentials -->
			<a href="#" class="success" data-step="2" id="next">Next</a> |
			<a href="<?php echo URL::full().'&step=cancel'; ?>" class="error">skip all</a>
		</div> <!-- end of forms -->
	</div>
	</div>	


	
</div>

@stop
@include('templates/bottom-admin')