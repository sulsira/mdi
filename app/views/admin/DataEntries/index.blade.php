<?php #page specific processing

?>
@include('templates/top-admin')
@section('content')
<div class="cc">
		<div class="pre-container">
			<div class="pre-header">
				<h2>Adding new Student</h2>
				<p>In this section you add student bio data and status</p>
			</div>
			<hr>
			<div class="pre-content">
				<div class="pch">
					<ul>
						<li class="active">
							<span class="pre-icon mega-octicon octicon octicon-file-media"></span>
							<strong class="step"><a href="#">Applicant</a></strong>
							add: schools attended an certificates
						</li>
						<li>
							<span class="pre-icon mega-octicon octicon octicon-book"></span>
							<strong class="step"><a href="">Work</a></strong>
							Add: work places, positions held, duration
						</li>
						<li>
							<span class="pre-icon mega-octicon octicon octicon-briefcase"></span>
							<strong class="step"><a href="">Student</a></strong>
							Add: intervied on, course payment
						</li>
						<li></li>
					</ul>
				</div>
				<div class="forms">
					<div class="add-academics spacer" >
					<h3>Student name here</h3>	
					{{Form::open(array('route'=>'data_entry.store','files'=>true,'class'=>'tenant'))}}
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th colspan="2"  rowspan = "1"><a href="" id="sname" class="trigger" data-type="add-Compound" data-content="forming" data-container="add-academics"><span class="mega-octicon octicon-plus"></span></a>Name of School / College / University</th>
								<th colspan="3">Dates</th>
							</tr>
							<tr>
								<th  colspan="2"></th>
								<!-- <th></th> -->
								<th>From</th>
								<th>To</th>
							</tr>
						</thead>
						<tbody>			
							<tr class="forming">
								<td colspan="2">
									{{Form::text('Compound_location',null,['placeholder'=>'Add Compound location','required'=>1,'class'=>"school-name",'autofocus'])}}
								</td>

								<td>
									{{Form::text('fullname',null,['placeholder'=>'land lord name','required'=>1])}}
								</td>
		
								<td class="last">
									{{Form::text('contacts',null,['placeholder'=>'Add contact info','required'=>1])}}
									<button type="submit" class="add-mit-right" title="add a Compound you have attended" ><span class="mega-octicon octicon-plus"></span></button>
									<div class="add-doc">
										<span class="octicon octicon-triangle-up"></span>
										<hr>
										{{Form::file('doc',null,['placeholder'=>'Add Docuemnt'])}}
										 <span class="help-block">Only pdf, img, docs.</span>
										<hr>
										<button type="submit" class="btn btn-info">upload a document</button>
									</div>
									<div class="add-subject">
										<span class="octicon octicon-triangle-up"></span>
										<hr>
										<h3>certificate name</h3>
										<hr>
										<div class="subgrad">
											<table>
												<thead>
													<tr>
														<th>Subjects</th>
														<th>Grade</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<td rowspan="2">
															<button type="submit" class="btn btn-info">Save subjects</button>
														</td>
													</tr>
												</tfoot>
												<tbody>
													<tr>
														<td><input type="text"></td>
														<td><input type="text"></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					{{Form::close()}}
					</div> <!-- end of academics -->
					<div class="add-academics spacer work" style="display:none">
					<h3>Land</h3>	
					{{Form::open(array('route'=>'data_entry.store','files'=>true,'class'=>'upload-prev','id'=>'work'))}}
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th colspan="5"><a href="" id="sname" class="trigger" data-type="add-Compound" data-content="forming" data-container="add-academics"><span class="mega-octicon octicon-plus"></span></a>Plot</th>
							</tr>
							<tr>
								<th>name</th>
								<th>address/location</th>
								<th>number(D)</th>
								<th>Price</th>

<!-- 								<th>address</th>
								<th></th> -->
							</tr>
						</thead>
						<tbody>			

							<tr class="forming">
								<td>{{Form::text('Compound_name',null,['placeholder'=>'Add Compound name','required'=>1,'autofocus'])}}</td>
								<!-- <td>s</td> -->
								<td>{{Form::text('Compound_name',null,['placeholder'=>'Add Compound location','required'=>1])}}</td>
								<td>
									{{Form::number('Compound_name',null,['placeholder'=>'Add Compound of houses','step'=>'any'])}}

								</td>
								<td class="last">
									{{Form::text('certificate_name',null,['placeholder'=>'Add contact info','required'=>1])}}
									<button type="submit" class="add-submit-right" title="add a Compound you have attended" ><span class="mega-octicon octicon-plus"></span></button>
									<div class="add-doc">
										<span class="octicon octicon-triangle-up"></span>
										<hr>
										{{Form::file('doc',null,['placeholder'=>'Add Docuemnt'])}}
										 <span class="help-block">Only pdf, img, docs.</span>
										<hr>
										<button type="submit" class="btn btn-info">upload a document</button>
									</div>
									<div class="add-subject">
										<span class="octicon octicon-triangle-up"></span>
										<hr>
										<h3>certificate name</h3>
										<hr>
										<div class="subgrad">
											<table>
												<thead>
													<tr>
														<th>Subjects</th>
														<th>Grade</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<td rowspan="2">
															<button type="submit" class="btn btn-info">Save subjects</button>
														</td>
													</tr>
												</tfoot>
												<tbody>
													<tr>
														<td><input type="text"></td>
														<td><input type="text"></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					{{Form::close()}}
					</div> <!-- end of work -->
					<div class="add-academics spacer credentials">
					<h3>Credentials</h3>	
					{{Form::open(array('route'=>'data_entry.store','files'=>true,'class'=>'upload-prev'))}}
						<div class="input-prepend input-append">
						  <span class="add-on">Student Number/ID</span>
						  <input class="span2" id="appendedPrependedInput" type="text">
						  <span class="add-on">#</span>
						</div>
						<div class="form-actions">
						  <button type="submit" class="btn btn-info">Create</button>
						  <button type="button" class="btn btn-success">Generate</button>
						</div>
					{{Form::close()}}
					</div> <!-- end of credentials -->
			<a href="#">skip</a> |
			<a href="#">skip all</a>
		</div> <!-- end of forms -->
	</div>
	</div>	


	
</div>

@stop
@include('templates/bottom-admin')