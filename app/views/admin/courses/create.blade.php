@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="create-department">
			<div class="form-snippet">
				<div class="form-header">
					<div class="title">
						<h2>Add A New Course </h2>
					</div>
				</div>
				<div class="messages">
					@include('__partials/errors');
					 @if(Session::has('success')) 
					  <h3 class="text-success">
					    You have successfully created: <strong> {{ucwords(Session::get('success'))}} </strong>
					  </h3>
					  <hr>
					 @endif
				</div>
				{{Form::open(['route'=>'departments.courses.store'],[],['class'=>'form-snippet'])}}

					<div class="level details">

						<div class="first">
							<div>
								{{Form::label('name','Course Name')}}
								{{Form::text('name',null,['class'=>'input-xlarge','placeholder'=>'Enter course name', 'required'=>1])}}
							</div>
							<div>
								{{Form::label('depart_id','Course Department')}}
								<select name="depart_id" id="nation" class="input-xlarge">
									<?php $dept = Department::all()->toArray();  ?>
										<?php if (!empty($dept)): ?>
											@foreach ($dept as $key => $value)
												<option value="{{$dept[$key]['id']}}">{{$dept[$key]['name']}}</option>
											@endforeach	
											<?php else: ?>
										<?php endif ?>
								</select>
							</div>
							<div>
								{{Form::label('staffID','Course Lecturer')}}
								<select name="staffID" id="nation" class="input-xlarge">
									<?php $lecturers = Person::lecturers();  ?>
									<?php foreach ($lecturers as $value): ?>
										<option value="{{$value['staff_id']}}"><?php echo ucwords(e($value['pers_fname'].' '. $value['pers_mname'] .' '.$value['pers_lname'])) ?></option>
									<?php endforeach ?>
								</select>
							</div>	
						</div>
					</div>
					<div class="level details">
						<span>Details </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('fname','Entry Qualification')}}
								{{Form::date('entry_qualification',['class'=>'input-xlarge','placeholder'=>'Enter admission date'])}}
							</div>
							<div>
								{{Form::label('qualification','Award')}}
								{{Form::text('qualification',null,['class'=>'input-xlarge','placeholder'=>'Enter award name'])}}
							</div>
							<div>
								{{Form::label('duration','Duration')}}
								{{Form::text('duration',null,['class'=>'input-xlarge','placeholder'=>'Enter duration in Months'])}}
							</div>
							<div>
								{{Form::label('fee','Fees')}}
								{{Form::text('fee',null,['class'=>'input-xlarge','placeholder'=>'Enter Fees in dalasis'])}}

							</div>
						</div>
					</div>
					<div class="level actions">
						<div>
							  <button type="submit" class="btn btn-large btn-primary span6" name="save" value="save">Add Course</button>
						</div>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')