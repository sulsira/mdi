<?php #page specific processing
	// dd($courses);

 ?>
@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Courses</h3>
	</div>
	<div class="cc">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Course Name</th>
					<th>Department</th>
					<th>Lecturer Qualification</th>
					<th>Fee</th>
					<th>Duration</th>
					<th>Award</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($courses)): ?>
					<?php foreach ($courses as $key => $value): ?>
					<tr>
						<td>{{$key+1}}</td>
						<td>{{ucwords($value['name'])}}</td>
						<td><a href="{{route('departments.show',$value['depart_id'])}}">{{@ucwords($value['department']['name'])}}</a></td>
						<td><a href="{{route('departments.staffs.show',[$value['depart_id'],$value['staffID']])}}">{{ucwords($value['staffs']['Staff_HighestQualification'])}}</a></td>
						<td><?php echo ($value['fee']) ?: 'N/A' ;?></td>
						<td><?php echo ($value['duration']) ?: 'N/A' ;?></td>
						<td><?php echo ($value['qualification']) ?: 'not specified' ;?></td>
						<td><a href="{{route('departments.courses.show',[$value['depart_id'],$value['id']])}}">View Course</a></td>
					</tr>						
					<?php endforeach ?>
					<?php else: ?>
					<tr>
						<td colspan="7"><h4>No Course Available!</h4></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
@stop
@include('templates/bottom-admin')