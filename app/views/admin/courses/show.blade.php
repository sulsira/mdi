<?php #page specific processing
$lecturer =  $course['staffs'];
	// dd($course);

 ?>
@include('templates/top-admin')
@section('content')
       <div id="scope">
          <div class="hedacont">
            <div class="navbar">
              <div class="navbar-inner" id="scopebar">
                  <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href=""><?php echo (ucfirst($course['name'])) ?: 'Course' ?></a>
                    <div class="nav-collapse collapse navbar-responsive-collapse">
                      <ul class="nav">  
                      	<li><a href="#details">Details</a> </li>
                      	<li><a href="#students">Students</a> </li>
                      	<li><a href="#students">Lecturers</a> </li>
                      	<li><a href="#students">Edit</a> </li>
                       </ul>
                    </div><!-- /.nav-collapse -->
                  </div>
              </div><!-- /navbar-inner -->
            </div>              
          </div>  
      </div>  <!-- end of scope -->
	<div class="c-header cc">
		<h3>Course</h3>
	<hr>
		<div id="details">
			<table class="table">
			  <thead>
			  	<tr>
			  		<th>Course name</th>
			  		<th>Lecturer name</th>
			  		<th>Qualification</th>
			  		<th>Fee</th>
			  		<th>Duration</th>
			  		<th>Created</th>
			  	</tr>
			  </thead>
			  <body>
			  <tr>
			  	<td><?php echo (ucfirst($course['name'])) ?: 'Course' ?></td>
			  	<td>
			  		@if(!empty($lecturer))
			  		{{$lecturer['persons']['pers_fname'].' '.$lecturer['persons']['pers_mname'].' '.$lecturer['persons']['pers_lname']}} 	
			  		@else
			  		-
			  		@endif
			  	</td>
			  	<td>
			  		
			  			{{$lecturer['Staff_HighestQualification']}}
			  		
			  	</td>
			  	<td>
			  		
			  			{{$course['fee']}}
			  		
			  	</td>
			  	<td>
			  		
			  			{{$course['duration']}}
			  		
			  	</td>
			  	<td>
			  		
			  			{{$course['created_at']}}
			  		
			  	</td>
			  </tr>


			  </body>
			</table>
		</div>
	</div>
	<div class="cc">
		<?php $students = $course['students']; ?>
		<div class="tabbable">
		  <ul class="nav nav-tabs">
			<?php
			 $count = 0 ;
			 $keys = array();
			 ?>
			<?php if (!empty($students)): ?>
				<?php foreach ($students as $key => $value): ?>	
				 <?php
				  	#$count++;
				   $keys[] = $key; 
				   asort($keys);
				   ?>
				<?php endforeach ?>  				
			<?php endif ?>

			<?php if (!empty($keys)): ?>
			<li <?php echo ($count == 0)? ' class="active"': ''; ?>><a href="#all-years" data-toggle="tab"><?php echo min($keys) .' To '. max($keys). ' Students';?></a></li>
						<?php foreach ($keys as $key): ?>
							<li><a href="#{{$key}}" data-toggle="tab">{{$key}}</a></li>
							<?php $count++; ?>
						<?php endforeach ?>	
			<?php endif ?>
		  </ul>
		<div class="tab-content">
			<i class="fa fa-refresh fa-spin" style="font-size: 128px; display:none; margin:20px 540px;color:#0071FF;"></i>
		  	<?php $counter = 0; ?>
		  	<div class="tab-pane active" id="all-years">
				<div id="students">
					<table class="table">
					  <thead>
					  	<tr>
					  		<th>Student name</th>
					  		<th>Admission Date</th>
					  		<th>Birthday</th>
					  		<th>Gender</th>
					  		<th>Nationality</th>
					  		<th>Actions</th>
					  	</tr>
					  </thead>
					  <tbody>
					  	<?php if (!empty($students)): ?>
					  		<?php foreach ($students as $key => $value): ?>
					  			<tr>
					  				<td><?php echo $value['persons']['pers_fname'].' '.$value['persons']['pers_mname'].' '.$value['persons']['pers_lname']; ?></td>
					  				<td><?php echo $value['admission_date']; ?></td>
					  				<td><?php echo $value['persons']['pers_DOB']; ?></td>
					  				<td><?php echo $value['persons']['pers_gender']; ?></td>
					  				<td><?php echo $value['persons']['pers_nationality'];?></td>
					  				<td><a href="{{route('courses.students.show',[$course['id'],$value['id']])}}">view student</a></td>
					  			</tr>					  			
					  		<?php endforeach ?>
				  		<?php else: ?>
				  		<tr>
				  			<td colspan="8">No Data is available!</td>
				  		</tr>
					  	<?php endif ?>
					  </tbody>
					</table>
				</div>		  		
		  	</div>
		</div>		
	</div>
@stop
@include('templates/bottom-admin')