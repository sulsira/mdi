<?php 	#page specifig procssing
// var_dump($data);

// die()	;
 ?>
@include('templates/top-admin')
@section('content')
	<div class="cc">
	<section>
		<?php if (!empty($data)): ?>
			<div class="stats">	
				<a href="#">fetch results</a>
			</div>	
			<hr>
			<ul class="search-results">
				<?php foreach ($data as $key => $value): ?>
				<?php if ($key == 'person' ): ?>
					<?php foreach ($value as $key1 => $value1): ?>
							<?php if (isset($value1['students'])): ?>
						<li class="result">
							<div class="search-detail">
							<strong>
								<?php $perstype = (isset($value1['pers_type']) && $value1['pers_type'] == 'Staff')? 'staffs' : 'students'; ?>

							<a href="<?php echo $perstype.'/'.$value1['id'] ?>"><?php echo $value1['pers_fname'].' '.$value1['pers_mname'].' '.$value1['pers_lname']; ?></a>
							</strong>
							<p>
								<span>Person Type : </span> <strong>{{$value1['pers_type']}}</strong> |
								<span>Nationality : </span> <strong>{{$value1['pers_nationality']}}</strong> |
								<span>Gender : </span> <strong>{{$value1['pers_gender']}}</strong> |
							</p>
							</div>
							<hr>
						</li>
						<?php endif ?>
						<?php if (	isset($value1['staffs'])): ?>

						<li class="result">
							<div class="search-detail">
							<strong>
								<?php $perstype = (isset($value1['pers_type']) && $value1['pers_type'] == 'Staff')? 'staffs' : 'students'; ?>

	<a href="<?php echo $perstype.'/'.$value1['staffs']['staff_id'] ?>"><?php echo $value1['pers_fname'].' '.$value1['pers_mname'].' '.$value1['pers_lname']; ?></a>
							</strong>
							<p>
								<span>Person Type : </span> <strong>{{$value1['pers_type']}}</strong> |
								<span>Nationality : </span> <strong>{{$value1['pers_nationality']}}</strong> |
								<span>Gender : </span> <strong>{{$value1['pers_gender']}}</strong> |
							</p>
							</div>
							<hr>
						</li>								
						<?php endif ?>
						
					<?php endforeach ?>
				<?php endif ?>
				<?php if ($key == 'department' ): ?>
				<?php foreach ($value as $ind => $depart): ?>
					<li class="result">
						<div class="search-detail">
						<strong>
							<a href="{{route('departments.index')}}">{{e(ucwords($depart['name']))}}</a>
						</strong>
						<p>
							<span>Result Type : </span> <strong>{{e($key)}}</strong> |
							<span>Person in charge : </span> <strong>{{e($depart['personName'])}}
						</p>
						</div>
						<hr>
					</li>						
				<?php endforeach ?>
				
				<?php endif ?>
				<?php if ($key == 'contact' ): ?>
					<?php foreach ($value as $ind => $cont): ?>
					<?php $perstype = ($cont['persons']['pers_type'] == 'Staff')? 'staffs' : 'students'; ?>
						<li class="result">
							<div class="search-detail">
							<strong>
								<a href="<?php echo 'search/persons?pers='.$cont['persons']['pers_type'].'&key='.$cont['persons']['id']; ?>">
								<?php echo ucwords($cont['persons']['pers_fname'].' '.$cont['persons']['pers_mname'].' '.$cont['persons']['pers_lname']); ?>
								</a>
							</strong>
							<p>
								<span>Contact Type : </span> <strong>{{$cont['Cont_ContactType']}}</strong> |
								<span>Contact Info : </span> <strong>{{$cont['Cont_Contact']}}</strong> 
							</p>
							<p>
								<span>Person Type : </span> <strong>{{$cont['persons']['pers_type']}}</strong> |
								<span>Nationality : </span> <strong>{{$cont['persons']['pers_nationality']}}</strong> |
								<span>Gender : </span> <strong>{{$cont['persons']['pers_gender']}}</strong> |
							</p>
							</div>
							<hr>
						</li>						
					<?php endforeach ?>
					
				<?php endif ?>
				<?php if ($key == 'course' ): ?>
				<?php foreach ($value as $key => $course): ?>
					<li class="result">
						<div class="search-detail">
						<strong>
							<a href="{{route('departments.courses.show',[$course['depart_id'],$course['id']])}}">{{e(ucwords($course['name']))}}</a>
						</strong>
						<p>
							<span>Search Type : </span> <strong>Course</strong> |
						</p>
						</div>
						<hr>
					</li>					
				<?php endforeach ?>
					
				<?php endif ?>					
				<?php endforeach; ?>				
			</ul>
			<div class="stats">	
					<a href="#">fetch results</a>
			</div>	
			<?php else: ?>
				<h3>Sorry there were no results</h3>
		<?php endif ?>

	</section>
	</div>
@stop
@include('templates/bottom-admin')