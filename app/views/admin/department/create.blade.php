@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="create-department">
			<div class="form-snippet">
				<div class="form-header">
					<div class="title">
						<h2>Create A New Department </h2>
					</div>
				</div>
				<div class="messages">
					@include('__partials/errors');
					 @if(Session::has('success')) 
					  <h3 class="text-success">
					    You have successfully created: <strong> {{ucwords(Session::get('success'))}} </strong>
					  </h3>
					  <hr>
					 @endif
				</div>
				{{Form::open(['route'=>'departments.store'],[],['class'=>'form-snippet'])}}
					<div class="level name">
						<div>
							{{Form::label('name','Department Name')}}
							{{Form::text('name',null,['class'=>'input-xlarge span6','placeholder'=>'Enter the department name','required'=>1])}}
						</div>
						<div>
							{{Form::label('personName','Person In-charge Name')}}
							{{Form::text('personName',null,['class'=>'input-xlarge span6','placeholder'=>'The name of the person incharge'])}}
						</div>
					</div>
					<div class="level actions">
						<div>
							  <button type="submit" class="btn btn-large btn-primary span6" name="save" value="save">Create Department</button>
						</div>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')