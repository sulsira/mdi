@include('templates/top-admin')
@section('content')
<div class="c-header cc">
		<h3>Staff</h3>
</div>
	<div class="cc">
		<div class="tabbable">
		  <ul class="nav nav-tabs">
			<?php
			 $count = 0 ;
			 $keys = array();
			 ?>
			<?php foreach ($staff as $key => $value): ?>	
			 <?php
			  	#$count++;
			   $keys[] = $key; 
			   asort($keys);
			   ?>
			<?php endforeach ?>  
			<?php if (!empty($keys)): ?>
			<li <?php echo ($count == 0)? ' class="active"': ''; ?>><a href="#all-years" data-toggle="tab">All Departments</a></li>
						<?php foreach ($keys as $key): ?>
							<li><a href="#{{$key}}" data-toggle="tab">{{$key}}</a></li>
							<?php $count++; ?>
						<?php endforeach ?>	
			<?php endif ?>
		  </ul>
		<div class="tab-content">
			<i class="fa fa-refresh fa-spin" style="font-size: 128px; display:none; margin:20px 540px;color:#0071FF;"></i>
		  	<?php $counter = 0; ?>
		  	<div class="tab-pane active" id="all-years">
				<div id="students">
					<table class="table table-bordered">
					  <thead>
					  	<tr>
					  		<th>Staff name</th>
					  		<th>Birthday</th>
					  		<th>Gender</th>
					  		<th>Nationality</th>
					  		<th>Staff Rank</th>
					  		<th>Role</th>
					  		<th>Hire Date</th>
					  		<th>Qualification</th>
					  		<th>Actions</th>
					  	</tr>
					  </thead>
					  <tbody>
					  	<?php if (!empty($staff)): ?>
					  	<?php foreach ($staff as $key => $value): ?>
					  		<?php foreach ($value as $ky => $val): ?>
		  			<tr>
		<td>{{ucwords($val['persons']['pers_fname'].' '.$val['persons']['pers_mname'].' '.$val['persons']['pers_lname'])}}</td>
		<td>{{ucwords($val['persons']['pers_DOB'])}}</td>
		<td>{{ucwords($val['persons']['pers_gender'])}}</td>
		<td>{{ucwords($val['persons']['pers_nationality'])}}</td>
		<td>{{ucwords($val['Staff_Rank'])}}</td>
		<td>{{ucwords($val['Staff_Role'])}}</td>
		<td><?php echo ($val['Staff_HireDate']) ?: 'N/A';?></td>
		<td><?php echo ($val['Staff_HighestQualification']) ?: 'N/A';?></td>
		<td><a href="{{route('staffs.show',$val['staff_id'])}}">View Staff</a></td>
		  			</tr>
					  		<?php endforeach ?>
					  	<?php endforeach ?>
						  	<?php else: ?>
					  		<tr>
					  			<td colspan="9">No Data is available!</td>
					  		</tr>					  		
					  	<?php endif ?>

					  </tbody>
					</table>
				</div>		  		
		  	</div>
		</div>		
	</div>
	</div>
@stop
@include('templates/bottom-admin')