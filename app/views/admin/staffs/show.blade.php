<?php #page specific processing

    if(isset($staffs) && !empty($staffs)):
        $staff_id = $staffs['staff_id'];
        $fullname = $staffs['persons']['pers_fname']. ' '.$staffs['persons']['pers_mname'].' '.$staffs['persons']['pers_lname'];
        $image = $staffs['persons']['document']['doc_fullpath'];
        $thumbnail = $staffs['persons']['document']['doc_extension'].'/'.$staffs['persons']['document']['doc_filename'];
    endif;

 ?>
@include('templates/top-admin')
@section('content')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Staff Name : {{$fullname}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                          	<li><a href="#index">Bio</a> </li>
                          	<li><a href="#academic">Qualifications</a> </li>
                            <li><a href="#transcript">Courses</a> </li>
                          	<li><a href="#docs">Document</a> </li>
                          	<li><a href="#students">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <ul class="thumbnails" id="thmb">
                    <li class="span2">
                      <a href="#" class="thumbnail">
                        <img src="<?php echo '../'.$thumbnail; ?>" data-src="holder.js/300x200" alt="">
                      </a>
                    </li>
                </ul>  
            </div>           
        </div>  
    </div>  <!-- end of scope -->
    <div  id="index"></div>
    <div class="cc">
        <!-- <hr> -->

        <div>
        <hr>
            <div class="bio">
                <div class="ch">
                    <h4 id="bio">Bio</h4>
                </div>
                <hr>
                <div class="details">
                    <div class="aside left span12">
                        <center>
                            <strong>{{ucwords("$fullname ")}}</strong>
                            <hr>
                            <div class="thumbnail span3 thm">
                            @if($image)
                                <img src="<?php echo '../'.$image; ?>" data-src="holder.js/300x200" alt="">
                            @else
                                <img src="{{'http://lorempixel.com/g/200/200/'}}" data-src="holder.js/300x200" alt="">
                            @endif
                                
                            </div>
                            <hr>
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($fullname)}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($staffs['persons']['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($staffs['persons']['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($staffs['persons']['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($staffs['persons']['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Qualification</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($staffs['contacts'])): ?>
                                            <?php foreach ($staffs['contacts'] as $key => $value): ?>
                                                <tr>
                                                    <td><?php echo ucwords($value['Cont_ContactType']); ?>:</td>
                                                    <td><?php echo ucwords($value['Cont_Contact']); ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Courses</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($staffs['courses'])): ?>
                                            <?php foreach ($staffs['courses'] as $key => $value): ?>
                                                <tr>
                                                    <td>name:</td>
                                                    <td><?php echo $value['name']; ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($staffs['addresses'])): ?>
                                            <?php foreach ($staffs['addresses'] as $key => $value): ?>
                                                <tr>
                                                    <td>Town:</td>
                                                    <td><?php echo  $value['Addr_Town']; ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr> 
                                                <tr>
                                                    <td>Region:</td>
                                                    <td><?php echo  $value['Addr_District']; ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>P.o.box:</td>
                                                    <td><?php echo  $value['Addr_POBox']; ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                               
                                            <?php endforeach ?>         
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </center>
                    </div>

                </div>
            </div>  <!--#bio--> 
        </div> <!-- #index -->
    </div> <!-- a .cc -->
@stop
@include('templates/bottom-admin')