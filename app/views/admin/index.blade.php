@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="icons_zone">
		
			<p>
				<a href="departments" class="imgthumb">
					<i class="fa fa-university"></i> 
				</a>
				<a href="departments">Departments</a>
			</p>
			<p>
				<a href="courses" class="imgthumb">
					<i class="fa fa-book"></i>
				</a>
				<a href="courses">Courses</a>
			</p>
			<p>
				<a href="staffs" class="imgthumb">
					<i class="fa fa-pencil"></i>
				</a>
				<a href="staffs">Staff</a>
			</p>
			<p>
				<a href="students" class="imgthumb">
					<i class="fa fa-child"></i>
				</a>
				<a href="students">Students</a>
			</p>
		</div><!-- content conent -->	
	</div>
@stop
@include('templates/bottom-admin')