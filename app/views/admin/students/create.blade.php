@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="create-department">
			<div class="form-snippet">
				<div class="form-header">
					<div class="title">
						<h2>Add A New Student </h2>
					</div>
				</div>
					<div class="messages">
						@include('__partials/errors');
						 @if(Session::has('success')) 
						  <h3 class="text-success">
						    You have successfully created: <strong> {{ucwords(Session::get('success'))}} </strong>
						  </h3>
						  <hr>
						 @endif
					</div>
				{{Form::open(['route'=>'courses.students.store', 'files'=>true],[],['class'=>'form-snippet'])}}

					<div class="level details">

						<div class="first">
							<div>
								{{Form::label('person[fname]','First Name')}}
								{{Form::text('person[fname]',null,['class'=>'input-xlarge','placeholder'=>'Enter first name','required'=>1])}}
							</div>
							<div>
								{{Form::label('person[mname]','Middle Name')}}
								{{Form::text('person[mname]',null,['class'=>'input-xlarge','placeholder'=>'Enter middle name'])}}
							</div>
							<div>
								{{Form::label('person[lname]','Last Name')}}
								{{Form::text('person[lname]',null,['class'=>'input-xlarge','placeholder'=>'Enter last name','required'=>1])}}
							</div>	

						</div>
					</div>
					<div class="level details">
						<span>Bio </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('person[dob]','Birthday')}}
								{{Form::date('person[dob]',['class'=>'input-xlarge','placeholder'=>'Enter admission date','required'=>1])}}
							</div>
							<div>
								{{Form::label('person[Pers_Ethnicity]','Ethniticity')}}
								<select name="person[Pers_Ethnicity]" id="enit" class="input-xlarge">
									<?php $countries = Variable::domain('Pers_Ethnicity')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
								</select>
							</div>
							<div>
								{{Form::label('person[Pers_Nationality]','Nationality')}}
								<select name="person[Pers_Nationality]" class="input-xlarge">
									<?php $countries = Variable::domain('Country')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
								</select>
							</div>
							<div>
								{{Form::label('person[Pers_Gender]','Gender')}}
								{{Form::select('person[Pers_Gender]', array('male' => 'Male','female' => 'Female'));}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Address</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('address[Addr_Town]','Town')}}
								{{Form::text('address[Addr_Town]',null,['class'=>'input-xlarge','placeholder'=>'Enter town'])}}
							</div>
							<div>
								{{Form::label('address[Addr_District]','District')}}
								<select name="address[Addr_District]" id="region">
									<?php $countries = Variable::domain('Addr_District')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
								</select> 
							</div>
							<div>
								{{Form::label('address[Addr_AddressStreet]','street')}}
								{{Form::text('address[Addr_AddressStreet]',null,['class'=>'input-xlarge','placeholder'=>'Enter street'])}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Contact</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('contact[phones]','Phones')}}
								{{Form::text('contact[phones]',null,['class'=>'input-xlarge','placeholder'=>'Enter phone number(S)'])}}
							</div>
							<div>
								{{Form::label('contact[email]','Email')}}
								{{Form::text('contact[email]',null,['class'=>'input-xlarge','placeholder'=>'Enter Email address'])}} 
							</div>
							<div>
								{{Form::label('contact[telephone]',' Telephone (land line)')}}
								{{Form::text('contact[telephone]',null,['class'=>'input-xlarge','placeholder'=>'Enter numbers'])}}
							</div>
						</div>
					</div>

					<div class="level details">
					
						<span>Contact person / Parent / Guardian</span>
						<hr>
						<div class="first ">
								{{Form::label('contact[parent_fullname]','Fullname')}}
								{{Form::text('contact[parent_fullname]',null,['class'=>'input-xlarge','placeholder'=>'Please enter parent fullname'])}}
							</div>
							<div>
								{{Form::label('contact[parent_contact]','Tel. No')}}
								{{Form::text('contact[parent_contact]',null,['class'=>'input-xlarge','placeholder'=>'Enter Telephone number'])}} 
							</div>

					</div>
					
					<div class="level details">
						<span>Please indicate if you are : </span>
						<hr>
						<div class="first">
							<div>
								{{Form::label('applicant[student_status]','Student status')}}
								<select name="applicant[student_status]"  class="input-xlarge">
									<option>New student</option>
									<option>Transferee</option>
									<option>Old Student</option>
								</select>
							</div>
							<div>
								{{Form::label('applicant[semister]','Semister')}}
								<select name="applicant[semister]" class="input-xlarge">
									<option value="1">Januray to June (<?php echo DATE("Y") ?>)</option>
									<option value="2">July to December (<?php echo DATE("Y") ?>)</option>
								</select>
							</div>
						</div>
					</div>

					<div class="level details">
						<span>Course</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('student[admission_date]','Admission')}}
								{{Form::date('student[admission_date]',['class'=>'input-xlarge','placeholder'=>'Enter admission date'])}}
							</div>
							<div>
								{{Form::label('student[course_id]','Course')}}
								<select name="student[course_id]" class="input-xlarge">
									<?php $course = Course::all();  ?>
									<?php foreach ($course as $key => $value): ?>
										<option value="{{$value['id']}}">{{$value['name']}}</option>
									<?php endforeach ?>
								</select>
							</div>

							<div>
								{{Form::label('student[course_level]','Course Level')}}
								<select name="student[course_level]" class="input-xlarge">
									<option>Cert conificate</option>
									<option>Deploma</option>
								</select>
							</div>

						</div>
					</div>

					<div class="level details">
					<hr>
					<span>Profile Picture</span>
			              <div class="sek">
			                <div class="input-group">
			                  <div class="input-group-addon">Photo</div>
			                  <input placeholder="name here" name="profilePic" type="file" class="form-control">
			                </div>            
			              </div>						

					</div>
					<div class="level actions">
						<div>
							<button type="submit" class="btn btn-large btn-primary span6" name="save" value="save">Add Student</button>
						</div>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')