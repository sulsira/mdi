@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="pre-container">
			<div class="pre-header">
				<h2>The student name</h2>
				<p> lorem lorem lorem lorem </p>
			</div>
			<hr>
			<div class="pre-content">
				<div class="pch">
					<ul>
						<li class="active">
							<span class="pre-icon mega-octicon octicon octicon-file-media"></span>
							<strong class="step">Phone </strong>
							passport size phote (profile picture)
						</li>
						<li>
							<span class="pre-icon mega-octicon octicon octicon-book"></span>
							<strong class="step">Academics </strong>
							passport size phote (profile picture)
						</li>
						<li>
							<span class="pre-icon mega-octicon octicon octicon-briefcase"></span>
							<strong class="step">Work</strong>
							passport size phote (profile picture)
						</li>
						<li>
							<span class="pre-icon mega-octicon octicon octicon-key"></span>
							<strong class="step">Credentials </strong>
							passport size phote (profile picture)
						</li>
					</ul>
				</div>
				

				<div class="forms">
					<div class="upload-pic spacer">
						<h3>Upload a picture</h3>
						{{Form::open(array('route'=>'students.store','files'=>true,'class'=>'upload-prev'))}}
						{{Form::file('something')}}
						{{Form::submit('something',null,["class"=>"btn"])}}
						{{Form::close()}}

						{{Form::open(array('route'=>'students.store','class'=>'upload-prev'))}}
						{{Form::label('something','The some')}}
						{{Form::text('something')}}
						{{Form::submit('something',null,["class"=>"btn"])}}
						{{Form::close()}}					</div>
					<div class="add-academics spacer">
					<h3>Academics</h3>	
					{{Form::open(array('route'=>'students.store','files'=>true,'class'=>'upload-prev'))}}
					<table class="table table-bordered">
						<thead>
							<tr>
								<th><a href="" id="sname"><span class="mega-octicon octicon-plus"></span></a>School name</th>
								<th colspan="2">Date</th>
								<th>Certificate</th>
							</tr>
							<tr>
								<th></th>
								<th>from</th>
								<th>to</th>
								<th></th>
							</tr>
						</thead>
						<tbody>	
						<tr>
							<td>two</td>
							<td>two</td>
							<td>two</td>
							<td class="last"><span>span</span> <a href="" title=" subjects and grades" class="add-more-right"><span class="mega-octicon octicon-plus"></span></a></td>
						</tr>						
							<tr class="forming">
								<td>{{Form::text('something')}}</td>
								<td>{{Form::text('something')}}</td>
								<td>{{Form::text('something')}}</td>
								<td class="last">
									{{Form::text('something')}}
									<button class="add-submit-right" title=" subjects and grades" ><span class="mega-octicon octicon-plus"></span></button>
									<div class="add-doc">
										<span class="octicon octicon-triangle-up"></span>
										<hr>
										{{Form::file('doc',null,['placeholder'=>'Add Docuemnt'])}}
										 <span class="help-block">Only pdf, img, docs.</span>
										<hr>
										<!-- {{Form::submit('doc',null, ['class'=>'btn btn-info'])}} -->
										<button type="submit" class="btn btn-info">upload a document</button>
									</div>
									<div class="add-subject">
										<span class="octicon octicon-triangle-up"></span>
										<hr>
										<h3>certificate name</h3>
										<hr>
										<button>done</button>
										<div class="subgrad">
											<table>

											</table>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					{{Form::close()}}
					</div>

				</div><hr>
		</div>
	</div>
@stop
@include('templates/bottom-admin')