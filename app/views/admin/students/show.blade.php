<?php #page specific processing
    $guardian = [];
    $academics = [];
    $employments = [];

    if(isset($data) && !empty($data)):
        $student_id = $data['id'];
        $fullname = $data['persons']['pers_fname']. ' '.$data['persons']['pers_mname'].' '.$data['persons']['pers_lname'];
        $image = $data['persons']['document']['doc_fullpath'];
        $thumbnail = $data['persons']['document']['doc_extension'].'/'.$data['persons']['document']['doc_filename'];

        $guardian = (isset($data['guardian']) && !empty($data['guardian']))? $data['guardian']: [];
        $academics = (isset($data['academics']) && !empty($data['academics']))? $data['academics']: [];
        $employments = (isset($data['employments']) && !empty($data['employments']))? $data['employments']: [];

    endif;
    // dd($data);
 ?>
@include('templates/top-admin')
@section('content')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Student Name : {{ucwords("{ $fullname }")}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                          	<li><a href="{{route('students.show',$student_id )}}#index">Bio</a> </li>
                          	<li><a href="{{route('students.show',$student_id )}}#academic">Academic Records</a> </li>
                            <li><a href="{{route('students.show',$student_id )}}#transcript">Employment</a> </li>
                          	<!-- <li><a href="#finance">Finance</a> </li> -->
                          	<li><a href="{{route('students.show',$student_id )}}#docs">Document</a> </li>
                          	<li><a href="{{route('students.edit', $student_id )}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <ul class="thumbnails" id="thmb">
                    <li class="span2">
                      <a href="#" class="thumbnail">
                        <img src="<?php echo '../'.$thumbnail; ?>" data-src="holder.js/300x200" alt="">
                      </a>
                    </li>
                </ul>  
            </div>           
        </div>  
    </div>  <!-- end of scope -->
    {{-- <div  id="index"></div> --}}
	<div class="cc clearfix">
        <!-- <hr> -->

    	<div>
        <hr>
    		<div class="bio">
                <div class="ch">
                    <h4 id="bio">Bio</h4>
                </div>
                <hr>
                <div class="details">
                    <div class="aside left span12">
                        <center>
                            <strong>{{ucwords("$fullname ")}}</strong>
                            <hr>
                            <div class="thumbnail span3 thm">
                            @if($image)
                                <img src="<?php echo '../'.$image; ?>" data-src="holder.js/300x200" alt="">
                            @else
                                <img src="{{'http://lorempixel.com/g/200/200/'}}" data-src="holder.js/300x200" alt="">
                            @endif
                                
                            </div>
                            <hr>
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($fullname)}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($data['persons']['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($data['persons']['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($data['persons']['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($data['persons']['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Guardian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty( $guardian )): ?>
                                                <tr>
                                                    <td><?php echo ucwords($guardian['fullname']); ?>:</td>
                                                    <td><?php echo ucwords($guardian['contact']); ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($data['contacts'])): ?>
                                            <?php foreach ($data['contacts'] as $key => $value): ?>
                                                <tr>
                                                    <td><?php echo ucwords($value['Cont_ContactType']); ?>:</td>
                                                    <td><?php echo ucwords($value['Cont_Contact']); ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Course Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($data['course'])): ?>
                                                <tr>
                                                    <td>name:</td>
                                                    <td>{{$data['course']['name']}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($data['addresses'])): ?>
                                            <?php foreach ($data['addresses'] as $key => $value): ?>
                                                <tr>
                                                    <td>Town:</td>
                                                    <td><?php echo  $value['Addr_Town']; ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr> 
                                                <tr>
                                                    <td>Region:</td>
                                                    <td><?php echo  $value['Addr_District']; ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>P.o.box:</td>
                                                    <td><?php echo  $value['Addr_POBox']; ?></td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                               
                                            <?php endforeach ?>         
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </center>
                    </div>

                </div>
    		</div>	<!--#bio-->	
    	</div> <!-- #index -->
    </div> <!-- a .cc -->
  <div class="cc" id="academic">
    <h3>Academic Records</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Schola name</th>
            <th>from</th>
            <th>to</th>
            <th>certificates</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($academics)): ?>
            <?php foreach ($academics as $key => $value): ?>
            <tr>
                <td>{{ucwords($value['aca_schoolname'])}}</td>
                <td>{{ucwords($value['aca_datefrom'])}}</td>
                <td>{{ucwords($value['aca_dateto'])}}</td>
                <td>
                    <?php if (isset($value['certificates'])): ?>
                        <ul>
                            <?php foreach ($value['certificates']as $k => $val): ?>
                                <li>{{HTML::link('/download?file='.urlencode($val['url']),'Download',['target'=>'_blank'])}}</li>
                            <?php endforeach ?>
                        </ul>
                    <?php endif ?>
                </td>
            </tr>                                               
            <?php endforeach ?>
        <?php endif ?>
      </tbody>
    </table>
  </div>

  <div class="cc" id="transcript">
    <h3>Employment</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Employer</th>

            <th>position held</th>
            <th>duties</th>
            <th>from</th>
            <th>to</th>
            <th>updated at</th>
        </tr>
      </thead>
      <tbody>
       
            <?php if (!empty($employments)): ?>
                <?php foreach ($employments as $key => $value): ?>
                   <tr> 
                   <td>{{$value['emp_employer']}}</td>
                   <td>{{$value['emp_positionHeld']}}</td>
                   <td>{{$value['emp_duties']}}</td>
                   <td>{{$value['emp_from']}}</td>
                   <td>{{$value['emp_to']}}</td>
                   <td>{{$value['updated_at']}}</td>
                   </tr>
                <?php endforeach ?>
                <?php else: ?>
                    <tr>
                      <td>Sorry no data yet!</td>   

                    </tr>
                   
            <?php endif ?>

       
      </tbody>
    </table>
  </div>

  <div class="cc" id="docs">
    <h3>Documents</h3>
    <hr>
    <table class="table table-bordered">
      <thead>
        <tr>
            <th>Document name</th>
            <th>Type</th>
            <th>update</th>
            <th>create</th>
            <th>action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td>some thing</td>
            <td>some thing</td>
            <td>some thing</td>
            <td>some thing</td>
            <td><a href="#">download</a>|<a href="#">view</a></td>
        </tr>
      </tbody>
    </table>
  </div>
@stop
@include('templates/bottom-admin')