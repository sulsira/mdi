@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Students</h3>
	</div>
	<div class="cc">
		<div class="tabbable">
		  <ul class="nav nav-tabs">
			<?php
			 $count = 0 ;
			 $keys = array();
			 ?>
			<?php foreach ($students as $key => $value): ?>	
			 <?php
			  	#$count++;
			   $keys[] = $key; 
			   asort($keys);
			   ?>
			<?php endforeach ?>  
			<?php if (!empty($keys)): ?>
			<li <?php echo ($count == 0)? ' class="active"': ''; ?>><a href="#all-years" data-toggle="tab"><?php echo min($keys) .' To '. max($keys). ' Students';?></a></li>
						<?php foreach ($keys as $key): ?>
							<li><a href="#{{$key}}" data-toggle="tab">{{$key}}</a></li>
							<?php $count++; ?>
						<?php endforeach ?>	
			<?php endif ?>
		  </ul>
		<div class="tab-content">
			<i class="fa fa-refresh fa-spin" style="font-size: 128px; display:none; margin:20px 540px;color:#0071FF;"></i>
		  	<?php $counter = 0; ?>
		  	<div class="tab-pane active" id="all-years">
				<div id="students">
					<table class="table table-bordered">
					  <thead>
					  	<tr>
					  		<th>Student name</th>
					  		<th>Admission Date</th>
					  		<th>Birthday</th>
					  		<th>Gender</th>
					  		<th>Nationality</th>
					  		<th>Actions</th>
					  	</tr>
					  </thead>
					  <tbody>
					  	<?php if (!empty($students)): ?>
					  		<?php foreach ($students as $key => $value): ?>
						  		<?php foreach ($value as $key1 => $value1): ?>
					  			<tr>
					  				<td><?php echo $value1['persons']['pers_fname'].' '.$value1['persons']['pers_mname'].' '.$value1['persons']['pers_lname']; ?></td>
					  				<td><?php echo $value1['admission_date']; ?></td>
					  				<td><?php echo $value1['persons']['pers_DOB']; ?></td>
					  				<td><?php echo $value1['persons']['pers_gender']; ?></td>
					  				<td><?php echo $value1['persons']['pers_nationality'];?></td>
					  				<td><a href="{{route('students.show',$value1['id'])}}">view student</a></td>
					  			</tr>					  			
						  		<?php endforeach ?>
					  		<?php endforeach ?>
				  		<?php else: ?>
				  		<tr>
				  			<td colspan="8">No Data is available!</td>
				  		</tr>
					  	<?php endif ?>
					  </tbody>
					</table>
				</div>		  		
		  	</div>
		</div>		
	</div>
	</div>
@stop
@include('templates/bottom-admin')