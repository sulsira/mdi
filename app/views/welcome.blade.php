@include('__partials/doc')
    {{ HTML::style('__public/css/main.css') }}
    {{ HTML::style('__public/css/start-main.css') }}
    {{ HTML::style('__public/css/start-style.css') }}	 
  	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<div class="wrapper">
		<div class="progress progress-striped active" style="margin:200px; z-index:10000; display:none;">
		   <div class="bar"></div>
		</div>
		<header>
			@yield('header')
		</header>
		<section>
			@yield('content')
		</section>	
		<footer>
			@yield('footer')
		</footer>	
	</div>
	<div class="scripts">
		
	</div>
</body>