@if(!empty($menu))
		<div class="fixheda">
			<div class="userheda fixheda_style">
				<div class="hedacont">
					<ul class="navigation">
					@foreach ($menu as $key => $value) 
						@if($value['visible'] == 1)
							@if($value['type'] == 'single')
								<li>{{HTML::link($value['url'],$value['name'],['class'=>'link'])}}</li>
							@endif
						@endif
					@endforeach
				</ul>	            
	            <div class="navbar" id="user_options">
					<ul class="nav pull-right">
					@foreach ($menu as $key => $value) 
						@if($value['visible'] == 1)
							@if($value['type'] == 'drop')
								<li class="dropdown">
									{{HTML::link($value['url'],$value['name'],['class'=>'dropdown-toggle usrname fix', 'data-toggle'=>"dropdown"])}}
										<ul class="dropdown-menu">
											@foreach($value['menu'] as $ke => $valu)
										<li>
											{{HTML::link($valu['url'],$valu['name'])}}<b class="caret"></b> 		
										</li>									
											@endforeach
									</ul>
								</li>
							@endif
						@endif
					@endforeach
					</ul>       								                 		            	
	            </div>
				</div>

		  		<div class="acctheda  fixheda_style">
					<div class="navbar">
					 	<div class="navbar-inner">
							<div class="hedacont">	
							    <a class="active brand blr" href="#" id="menu" title="Management Development Institution"><i class="fa fa-bars"></i></i>MDI</a>
							    <ul class="nav">
							      <li><a href="{{route('courses.students.create')}}">Add Student</a></li>
							      <li><a href="{{route('departments.staffs.create')}}">Add Staff</a></li>
							      <li><a href="{{route('departments.create')}}">Add Department</a></li>
							      <li><a href="{{route('departments.courses.create')}}">Add Course</a></li>
							      <li class="blr">
							      	<a href="#" id="checker">Check Student</a>
							      </li>
							    </ul>
							    	<div class="popover fade bottom in" id="studentChecker">
							      		<div class="arrow"></div>
							      		<h3 class="popover-title">Checking For A Student</h3>
							      		<div class="popover-content">
											<div class="tabbable"> <!-- Only required for left/right tabs -->
											  <ul class="nav nav-tabs">
											    <li class="active"><a href="#tab1" data-toggle="tab">Student ID</a></li>
											    <li><a href="#tab2" data-toggle="tab">Student Details</a></li>
											  </ul>
											  <hr>
											  <div class="tab-content">
											    <div class="tab-pane active" id="tab1">
									      			{{Form::open(array('action'=>'PagesController@postFetch','method'=>'POST'))}}
									      				<div class="input-prepend">
														  <span class="add-on">StudentID</span>
														  {{Form::text('studentID',null,['class'=>"span6",'placeholder'=>"Student ID",'required'=>1])}}
														</div>
													<div class="form-actions">
													  <button type="submit" class="btn btn-primary">Get Student</button>
													  <button type="button" class="btn">Cancel</button>
													</div>
									      			{{Form::close()}}
											    </div>
											    <div class="tab-pane" id="tab2">
									      			{{Form::open(array('action'=>'PagesController@postFind','method'=>'POST'))}}
									      				<div class="input-prepend">
									      				<label>Student Name :</label>
														  <span class="add-on">First</span>
														  {{Form::text('pers_fname',null,['class'=>"span2", 'placeholder'=>"name here"])}}
														 <span class="add-on">Middle</span>{{Form::text('pers_mname',null,['class'=>"span2", 'placeholder'=>"name here"])}}
														<span class="add-on">last</span>{{Form::text('pers_lname',null,['class'=>"span2", 'placeholder'=>"name here"])}}
														</div>
														<hr>
									      				<label>Student Details :</label>
									      				<div class="input-prepend">
														  <span class="add-on">Birthday</span>
										{{Form::date('pers_DOB',['class'=>"span2", 'placeholder'=>"Student birthday",'required'=>1])}}
														</div>
									      				<div class="input-prepend">
														  <span class="add-on">Course</span>
															<select name="course_id" class="input-xlarge">
																<?php $course = Course::all();  ?>
																<?php foreach ($course as $key => $value): ?>
																	<option value="{{$value['id']}}">{{$value['name']}}</option>
																<?php endforeach ?>
															</select>
														</div>
														<div class="form-actions">
														  <button type="submit" class="btn btn-primary">Get Student</button>
														  <button type="button" class="btn">Cancel</button>
														</div>
									      			{{Form::close()}}
											    </div>
											  </div>
											</div>
							      		</div>
							      	</div>
							    {{Form::open(array('route'=>'search.store','method'=>'get'))}}
									<div class="input-append">
										{{ Form::text('q',null,['class'=>'span5','placeholder'=>"seach here",'id'=>"appendedInputButtons",'required'=>1]) }}
										{{ Form::hidden('x',1) }}	
									  	<button class="btn" type="submit" name="type" value="natural">Search</button>
									</div>
								{{Form::close()}}
							</div>

						<div class="sidebar">
							<ul>
					            <li><a href="{{route('admin.index')}}" class="ui-link"><i class="fa fa-chevron-circle-left"></i>MDI info</a></li>
					            <li><a href="{{route('departments.create')}}" class="ui-link"><i class="fa fa-plus"></i>Departments</a></li>
								<li><a href="{{route('departments.staffs.create')}}" class="ui-link"><i class="fa fa-plus"></i>Staff</a></li>
					            <li><a href="{{route('departments.courses.create')}}" class="ui-link"><i class="fa fa-plus"></i>Courses</a></li>
					            <li><a href="{{route('courses.students.create')}}" class="ui-link"><i class="fa fa-plus"></i>Students</a></li>
					            <li><a href="#" class="ui-link"><i class="fa fa-cog"></i> Settings</a></li>
				            </ul>
						</div>
					</div>						  			
		  		</div>	
			</div>
			</div>	
		</div>
@endif