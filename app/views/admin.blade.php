@include('__partials/doc')
    {{ HTML::style('__public/css/main.css') }}
    {{ HTML::style('__public/css/style.css') }}  
    {{ HTML::style('__public/font-awesome-4.2.0/css/font-awesome.css') }}   
    {{ HTML::style('__public/css/octions/octicons.css') }}	 
</head>
<body>
	<div class="wrapper">
		<div class="progress progress-striped active" style="margin:200px; z-index:10000; display:none;">
		   <div class="bar"></div>
		</div>
		<header class="main-header">
			@include('__partials.menu');
			@yield('header')
		</header>
		<section>
			@yield('content')
		</section>	
		<footer>
			@yield('footer')
		</footer>	
	</div>
{{HTML::script('__public/js/jquery-1.9.1.js')}}
{{HTML::script('__public/js/bootstrap.js')}}
{{HTML::script('__public/js/bootstrap-tooltip.js')}}
{{HTML::script('__public/js/bootstrap-popover.js')}}
{{HTML::script('__public/js/bootstrap-alert.js')}}
{{HTML::script('__public/js/bootstrap-button.js')}}
{{HTML::script('__public/js/accordion.js')}}
{{-- {{HTML::script('__public/js/handlebars-v1.3.0.js')}} --}}
{{-- {{HTML::script('__public/js/complete.js')}} --}}
{{-- {{HTML::script('__public/js/realestate.js')}} --}}
{{HTML::script('__public/js/school-dataentry.js')}}
{{HTML::script('__public/js/imsky-holder-7978550/holder.js')}}
<script type="text/javascript">
  $('.tipify').tooltip();
  $(".alert").alert();
  $('.pressed').on('click',function(){
   $(this)
    .text($(this).data('loading-text'))
  });
  (function($){
  	$("#menu").on('click',function(){

    var $this = this;
	
    var $lefty = $( ".sidebar" ).show();   
    $lefty.animate({

      left: parseInt($lefty.css('left'),10) == 0 ? -$lefty.outerWidth() : 0

    },{
        duration: 330,
        complete: function(e){

            var here = this;
            $('html').click(function(){
            if ( parseInt($(here).css("left")) === 0 ) {
                $(here).animate({
                    left: parseInt($lefty.css('left'),10) == 0 ? -$lefty.outerWidth() : 0
                });        
            };
            });
            $(this).click(function(e){
                e.stopPropagation();
            });
               
        }
    });

  	});
  	$('#checker').popover();
  	$('#checker').on('click', function(){
  		$("#studentChecker").slideToggle(258);
  	});
    $(window).scroll(function(){
      if ($(this).scrollTop() > 40) {
        // console.log($(this).scrollTop())
      $('div#scope').addClass('scope');
      $('#thmb').css('float','right');
// $( "#thmb" ).animate({ "left": "-=50px" }, "slow" );
      }else{
      $('div#scope').removeClass('scope');
      $('#thmb').css('float','left');
      }
    });
    $('.cancel').on('click',function(e){
      var cid = $(this).data('containerid');
     $('#'+cid).slideUp(200);
      e.preventDefault();
    });
    
  })(jQuery);

  // Put event listeners into place
  // window.addEventListener("DOMContentLoaded", function() {
  //   // Grab elements, create settings, etc.
  //   var canvas = document.getElementById("canvas"),
  //     context = canvas.getContext("2d"),
  //     video = document.getElementById("video"),
  //     videoObj = { "video": true },
  //     errBack = function(error) {
  //       console.log("Video capture error: ", error.code); 
  //     };

  //   // Put video listeners into place
  //   if(navigator.getUserMedia) { // Standard
  //     navigator.getUserMedia(videoObj, function(stream) {
  //       video.src = stream;
  //       video.play();
  //     }, errBack);
  //   } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
  //     navigator.webkitGetUserMedia(videoObj, function(stream){
  //       video.src = window.webkitURL.createObjectURL(stream);
  //       video.play();
  //     }, errBack);
  //   }
  //   else if(navigator.mozGetUserMedia) { // Firefox-prefixed
  //     navigator.mozGetUserMedia(videoObj, function(stream){
  //       video.src = window.URL.createObjectURL(stream);
  //       video.play();
  //     }, errBack);
  //   }
  // }, false);


  // Trigger photo take
  // document.getElementById("snap").addEventListener("click", function() {
  //   context.drawImage(video, 0, 0, 640, 480);
  // });
 </script>
 <script>
(function($){
  // console.log(this);
  function checkUrl(){
    var url = $(this).prop('action');
    return url;
  };
  $('#next').on('click',function(e){
      var step = $(this).data('step');
    e.preventDefault();
   

    var lis = $(".steps").children("li");
    var active = lis.filter('li.active');
 
    active
      .removeClass('active');
      if(active.next().children().length == 0){
        lis.first().addClass('active');
        window.location.replace("../students/create");
      }else{
        active
          .next()
          .addClass('active');
          var at = $(active).data('stepid');
          var before = $(active).prev("li").data('stepid');
          // $(active).prev("li").hide();
          var hm = active.next();
          var hms = active;
          console.dir($("div#"+hms.data('stepid')).hide() );
          console.dir($("div#"+hm.data('stepid')).show() );
          // console.log();
     
      }
      
      
      console.log(lis.last().children().length == 0);
      // lis.eq(0).addClass('active');
    // // $(this)
    // //   .removeClass('active')
    // //   .end()
    // //   $('~ li')
    // //   .addClass('test')
    // //   if ( $(this).hasClass('active') ) {

    // //         console.log( $(this).next() );
    // //   //   $(this).removeClass('active');


    // //   //   if ($(this).next().children().length == null) {

    // //   //       $(".steps").children("li").first().addClass('active');

    // //   //   };
    // //   //     $(this).next().addClass('active');

    // //   }else{

    // //     // console.log( $(this).next() );
    // //   }
    //   // .find("li.active")

    //   // .end();
    // });
  });
})(jQuery);
 </script>
</body>