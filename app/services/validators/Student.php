<?php namespace services\validators;

class Student extends Validate{
		public static $rules = [
		'admission_date'=> 'date|max:200',
		'course_id'=> 'integer|max:200'
	];
	public function __construct($attributes = null){
		$this->attributes = $attributes ?: \Input::all();
	}
}