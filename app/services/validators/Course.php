<?php namespace services\validators;

class Course extends Validate{
		public static $rules = [
		'name'=> 'required|max:200',
		'depart_id'=> 'integer|max:200',
		'staffID'=> 'integer|max:200',
		'entry_qualification'=> 'max:200',
		'qualification'=> 'max:200',
		'duration'=> 'max:200',
		'fee'=> 'max:200'
	];
	public function __construct($attributes = null){
		$this->attributes = $attributes ?: \Input::all();
	}
}