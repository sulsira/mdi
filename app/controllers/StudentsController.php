<?php

class StudentsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /students
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array();
		$students = Student::with('persons')->get()->toArray(); //failure is bound to happen here
		if (!empty($students)) {
			foreach ($students as $key => $student) {
				$data[pull_year($student['admission_date'])][] = $student;
			}
		}

		$this->layout->content = View::make('admin.students.index')->with('students',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /students/create
	 *
	 * @return Response
	 */
	public function create()
	{
		
		$this->layout->content = View::make('admin.students.create');

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /students
	 *
	 * @return Response
	 */
	public function store()
	{

	$input = Input::all();
		// tables array

		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$applicant = [];
		$student = [];
		$done = false;
		// dd($input);
		// loop and look for values in the user sent input if not empty add to the above array
		// there is a problem with the middle name insert value
		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_indentifier' => 'null', 
							'pers_givenID' => 0, 
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Student', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							'pers_standing' => '0', 
							'pers_NIN' => 0,
							'pers_deleted'	 => '0'
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						if ($person->id) {
							
							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){
									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key,	
										));
									}
								}
							}





							$errors = $V->errors;
						}
					}
					var_dump($k == 'student');
					if ($k == 'student') {
						
						if ($person->id) {
							$V = new services\validators\Student($table);
							if($V->passes()){
								$student = Student::create([
									'admission_date' => $table['admission_date'],
									'course_id' => $table['course_id'],
									'person_id' => $person->id,
									'course_level_id' => 0
									]);

								if ($student) {
									$done = true;
								}
							}
							$errors = $V->errors;
						}
					}


				} #end of if table is array
			}#end of each 

			if($done){

				// start inserting parent and other new objects
			
						
				$applican = new Applicant;
				$applican->student_type = $input['applicant']['student_status'];
				$applican->std_id = $student['id'] ;
				$applican->person_id = $person->id;
				$applican->application_person = 'applicant';
				$applican->semister_admitted = $input['applicant']['semister'];
				$applican->save();
				$applicant = $applican;
			
				if(isset($student) && !empty($student)){


					Guardian::create( ['fullname' => $input['contact']['parent_fullname'], 'contact' => $input['contact']['parent_contact'], 'student_id'=> $student->id] );


				}




        // file pulling
        $main_dir = 'media/Students/'.$student->person_id;


        if($input['profilePic']){
            
            $img = Uploader::image($input['profilePic'])
                ->directory($main_dir)
                ->rename($student->person_id.'_'.'pp')
                ->resize(128,128, 'thumbsnails');
            	$image = $img->details(); 
            	Document::create([
						'doc_entityType' => 'Person',
						'doc_entityID' => $person->id,
						'doc_type' => 'image',
						'doc_fullpath' => $image['new']['file'],
						'doc_filename' => $image['new']['filename'],
						'doc_foldername' => $image['new']['folder'],
						'doc_extension'  => $image['new']['thumnail_location']
            	]);
             }

				Session::flash('success', "A Student");
				// $this->layout->content = View::make('admin.DataEnteries.index')->with('student',$student);
				return Redirect::route('data_entry.show', [ $student->id,"appID"=>$applicant->id ]);
			}else{

				return Redirect::back()->withErrors($errors)->withInput();	

			}
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /students/{id} 
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($stdID)
	{

		$student = array();
		$id = (Request::segment(4)) ?: $stdID;
		$student = Student::with('persons.document','course','academics.certificates','academics.grades','guardian','employments')->where('id','=',$id);

		$student = ($student->get()->toArray())? $student->first()->toArray(): [];
		// dd($student);
		// if($student):
		// 	$the_student = $student->toArray();
		// 	$student =& $the_student;
		// 	$student['courses'] = Course::with('Staffs')->where('staffID','=',$the_student['course_id'])->get()->toArray();
		// 	if( isset($the_student['persons']) && ($the_student['persons'] != null) ):
		// 		$student['contacts'] = Contact::whereRaw('Cont_EntityID = ? AND Cont_EntityType = ?',[$the_student['persons']['id'],'Person'])->get()->toArray();
		// 		$student['addresses'] = Address::whereRaw('Addr_EntityID = ? AND Addr_EntityType = ?',[$the_student['persons']['id'],'Person'])->get()->toArray();
		// 	endif;
		// endif;
		$this->layout->content = View::make('admin.students.show')->with('data',$student);
	}


	/**
	 * Show the form for editing the specified resource.
	 * GET /students/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// $student = array();
		// // $id = (Request::segment(4)) ?: $stdID;
		// $id = 4;
		// $student = Student::with('persons')->where('id','=',$id)->first();
		// if($student):
		// 	$the_student = $student->toArray();
		// 	$student =& $the_student;
		// 	$student['courses'] = Course::with('Staff')->where('staffID','=',$the_student['course_id'])->get()->toArray();
		// 	if( isset($the_student['persons']) && ($the_student['persons'] != null) ):
		// 		$student['contacts'] = Contact::whereRaw('Cont_EntityID = ? AND Cont_EntityType = ?',[$the_student['persons']['id'],'Person'])->get()->toArray();
		// 		$student['addresses'] = Address::whereRaw('Addr_EntityID = ? AND Addr_EntityType = ?',[$the_student['persons']['id'],'Person'])->get()->toArray();
		// 	endif;
		// endif;

		// $this->layout->content = View::make('admin.students.edit')->with('data',$student);

		$student = array();
		// $id = (Request::segment(4)) ?: $stdID;
		$student = Student::with('persons.document','course')->where('id','=',$id);

		$student = ($student->get()->toArray())? $student->first()->toArray(): [];
		// if($student):
		// 	$the_student = $student->toArray();
		// 	$student =& $the_student;
		// 	$student['courses'] = Course::with('Staffs')->where('staffID','=',$the_student['course_id'])->get()->toArray();
		// 	if( isset($the_student['persons']) && ($the_student['persons'] != null) ):
		// 		$student['contacts'] = Contact::whereRaw('Cont_EntityID = ? AND Cont_EntityType = ?',[$the_student['persons']['id'],'Person'])->get()->toArray();
		// 		$student['addresses'] = Address::whereRaw('Addr_EntityID = ? AND Addr_EntityType = ?',[$the_student['persons']['id'],'Person'])->get()->toArray();
		// 	endif;
		// endif;
		$this->layout->content = View::make('admin.students.edit')->with('data',$student);	}

	/**
	 * Update the specified resource in storage.
	 * PUT /students/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input  = Input::all();
		if(isset($input['type'])){
			if ($input['type'] == 'studentID') {
				$student = Student::findOrFail($id);
				$student->studentID = $input['student_number'];
				$student->save();

				return Redirect::to('students/create');
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /students/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}