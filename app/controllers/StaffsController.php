<?php
class StaffsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /staffs
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array();
		$staff = Staff::with('persons')->get()->toArray(); // could cause a serious {major problem i am using hasOne and hasOne insted of has$belong}
		if (!empty($staff)) {
			foreach ($staff as $key => $value) {
				$dep = Staff::find($value['staff_id'])->with('departments')->first()->toArray();
				if (isset($dep)) {
					$data[$dep['departments']['name']][] = $value;
				}
				$data['others'][] = $value;
			}
		}
		$this->layout->content = View::make('admin.staffs.index')->with('staff', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /staffs/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.staffs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /staffs
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		// tables array
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$done = false;
		// loop and look for values in the user sent input if not empty add to the above array
		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_indentifier' => 'null', 
							'pers_givenID' => 0, 
							'pers_fname' => $table['fname'], 
							'pers_mname' => $table['mname'], 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Staff', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							'pers_standing' => '0', 
							'pers_NIN' => 0,
							'pers_deleted'	 => '0'
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {
						if ($person->id) {
							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){
									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key,	
										));
									}
								}
							}
							$errors = $V->errors;
						}
						
					}
					if ($k == 'staff') {
						if ($person->id) {
							$V = new services\validators\Staff($table);
							if($V->passes()){
								$staff = Staff::create(array(
									'Staff_PersonID'=> $person->id,
									'Staff_Rank'=> $table['rank'],
									'Staff_HighestQualification'=> $table['hq'],
									'Staff_Role'=> $table['role'],
									'Staff_FieldOfTeaching'=> $table['deptID'],
									'Staff_MainLevelTeaching'=> $table['mtf'],
									'dept_id'=>$table['deptID']
								));
								if ($staff->Staff_PersonID) {

									        // file pulling
									        $main_dir = 'media/Staffs/'.$person->id;


									        if($input['profilePic']){
									            
									            $img = Uploader::image($input['profilePic'])
									                ->directory($main_dir)
									                ->rename($person->id.'_'.'ss')
									                ->resize(128,128, 'thumbsnails');
									            	$image = $img->details(); 
									            	Document::create([
															'doc_entityType' => 'Person',
															'doc_entityID' => $person->id,
															'doc_type' => 'image',
															'doc_fullpath' => $image['new']['file'],
															'doc_filename' => $image['new']['filename'],
															'doc_foldername' => $image['new']['folder'],
															'doc_extension'  => $image['new']['thumnail_location']
									            	]);
									             }

									$done = true;
								}
							}
							$errors = $V->errors;
						}

					}
				}
			}
			if($done){
				Session::flash('success', "A staff");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /staffs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$staffs = array();
		// $id = (Request::segment(3)) ?: $stdID;
		$staff = Staff::with('persons.document','courses.department','departments')->where('staff_id','=',$id);

		$staffs = ($staff->get()->toArray())? $staff->first()->toArray(): [];  
		$this->layout->content = View::make('admin.staffs.show',compact('staffs'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /staffs/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// 		$staffs = array();
		// // $id = (Request::segment(3)) ?: $stdID;
		// $staff = Staff::with('persons.document','courses.department','departments')->where('staff_id','=',$id);

		// $staffs = ($staff->get()->toArray())? $staff->first()->toArray(): [];  
		// $this->layout->content = View::make('admin.staffs.edit',compact('staffs'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /staffs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /staffs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}