<?php

class EmploymentsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /employments
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /employments/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /employments
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		if(Request::ajax()){

			if ($input['type'] == 'work') {
				
				$from = date("Y-m-d H:i:s", strtotime($input['from']));
				$to = date("Y-m-d H:i:s", strtotime($input['to']));
				$employ =  new Employment;
				$employ->emp_studentID = $input['studentID'];
				$employ->emp_employer = $input['employer'];
				$employ->emp_from = $from;
				$employ->emp_to = $to;
				$employ->emp_positionHeld = $input['position'];
				$employ->emp_duties = $input['duties'];
				$employ->save();
				return Response::json(  $employ  );
			
			}
		}
	}

	/**
	 * Display the specified resource.
	 * GET /employments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /employments/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /employments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /employments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}