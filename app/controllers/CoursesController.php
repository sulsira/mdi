<?php

class CoursesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /courses
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array();
		$course = Course::with('staffs');
		$c = (!empty($course))? $course->get()->toArray() : []; // future problem can break any time the returned data is blank
		if (!empty($c)) {
			foreach ($c as $key => $value) {
				$depart = Department::where('id','=',$value['depart_id']);

				if (!empty($depart)) {
					$c[$key]['department'] = ($depart->first() !== null)? $depart->first()->toArray() : [];
				}
			}
		}
		$this->layout->content = View::make('admin.courses.index')->with('courses',$c);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /courses/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.courses.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /courses
	 *
	 * @return Response
	 */
	public function store()
	{
		$input  = Input::get();
		// dd($input);
		$V = new services\validators\Course;
		if($V->passes()){
			$person = Course::create($input);
			Session::flash('success', "A Course");
			return Redirect::back();
		}else{
			$errors = $V->errors;
			return Redirect::back()->withErrors($errors)->withInput();							
		}

	}

	/**
	 * Display the specified resource.
	 * GET /courses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($in)
	{
		$data = array();
		$id = Request::segment(4);
		$default = array();
		$course = Course::with('students.persons', 'Staffs.persons')->where('id','=',$id); //should be control for deleted and hidden rows
		$acourse = array();
		if (!empty($course)) {
			$acourse = $course->first()->toArray();
		}
		// foreach ($acourse as $key => $value) {
		// 	$default = $value;
		// 	// $check =  Staff::lecturer($value['staffID'])->get();
		// 	$default['lecturers'] =  Staff::lecturer($value['staffID'])->get()->toArray();
		// 	// $default['lecturers'] =  Staff::with('person', function($query){
		// 	// 	$query->where('pers_type','=','Staff');
		// 	// })->where('dept_id','=','')->get();
		// 	// var_dump($value['staffID']);

		// 	foreach ($value['students'] as $key2 => $value2) {

		// 		if (!empty($value2['person_id'])) {
		// 			$std = array();
		// 			$std[pull_year($value2['admission_date'])][$key2] = $value2;
		// 			$pers =  Person::student($value2['person_id']); // security treat { if the person selected has no pers_type or is staff application will fail}
		// 			$std[pull_year($value2['admission_date'])][$key2]['details'] = ( !empty($pers) )? $pers->first()->toArray() : [];
		// 			$default['students'] = $std;
		// 		}
				
		// 	}
			
			
		// }
		$this->layout->content = View::make('admin.courses.show')->with('course',$acourse);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /courses/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /courses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /courses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}