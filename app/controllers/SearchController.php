<?php

class SearchController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /search
	 *
	 * @return Response
	 */
	private $keywords = '';


	/**
	 * Show the form for creating a new resource.
	 * GET /search/create
	 *
	 * @return Response
	 */

	/**
	 * Display the specified resource.
	 * GET /search/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// only people are filtered
		$input = Input::all(); #security loop hole
		if ($input['pers'] == 'Staff') {
			// do some processes
			$this->layout->content = View::make('admin.staffs.show');
		}
		if ($input['pers'] == 'Student') {
			// do some processes

			$this->layout->content = View::make('admin.students.show');
		}

	}

	public function index(){
		$input = Input::all();
		$this->type($input);
	}


	public function type($query)
	{
		$q = $query;

		$this->keywords = $q['q'];

		if (empty($q)) {
			var_dump('nothing to search for');
		}
		if (!empty($q['q'])) {

			if (!empty($q['type'])) {

				if ($q['type'] == 'natural') {

					$this->stringType(e($q['q']),$q['x']);

				}else if($q['type'] == 'index'){

						$this->indexSearch(e($q['q']));
				}
			}else{
				
				$this->stringType(e($q['q']));
			}
		}
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /search/create
	 *
	 * @return Response
	 */
	public function stringCreator()
	{
		return 'hello';
	}
	public function stringType($string)
	{
		$data_search = ['string'=>$string];
		if (str_contains($string,' ')) {
			// its means its a sentences or phrase
			$data_search = [
				'string' => $string,
				'tables' => ['persons','departments','courses']
			];
		}else{
			if (is_numeric($string)) {
				$data_search = [
					'string' => $string,
					'tables' => ['contacts','students']
				];
			}else{
			    if(filter_var($string, FILTER_VALIDATE_EMAIL)) {
			       		$data_search = [
					'string' => $string,
					'tables' => ['contacts']
					];
			    }

			    if(is_string($string)) {
			       	$data_search = [
					'string' => $string,
					'tables' => ['persons','departments','courses','contacts']
					];
			    }	

			}
		}

		$this->naturalSearch($data_search);
	}
	/**
	 * Store a newly created resource in storage.
	 * POST /search
	 *
	 * @return Response
	 */
	public function indexSearch($string)
	{
		// 
	}




	public function naturalSearch($search, $x = 0)
	{
		// $string = "jolloftutors@hotmail.com";
		// $string = "momodou jallow";
		$resutls_set = array();
		$string = $search['string'];
		$limit = 0 .','. 28;	
		if(is_numeric($x)){

			if ($x != 1) {

				$count = 10;
				$limit = ( 28 + (  $count * $x  ) ) .','. ( $count * $x );

			}
			
		}

		if (isset($search['tables'])) {
			foreach ($search['tables'] as $key => $value) {

					if ($key == 'persons') {
						$resutls_set['persons']= DB::select(DB::raw("
							(SELECT
								id AS primaryKey , pers_type AS entity,  @table := 'persons' AS tabal
								FROM persons
								WHERE MATCH(pers_fname,pers_mname,pers_lname)
								AGAINST('$string' IN BOOLEAN MODE)

						)"));
					}
					if ($key == 'departments') {
						$resutls_set['departments']= DB::select(DB::raw("
								(SELECT
									id AS primaryKey, name AS entity,personName AS type, @table := 'departments' AS tabal
									FROM departments
									WHERE MATCH(name,personName)
									AGAINST('$string' IN BOOLEAN MODE)
								)"));
					}
					if ($key == 'courses') {
						$resutls_set['courses']= DB::select(DB::raw("
								(SELECT
									id AS primaryKey,name  AS entity ,staffID  AS type, @table := 'courses' AS tabal
									FROM courses
									WHERE MATCH(name)
									AGAINST('$string' IN BOOLEAN MODE)
								)"));
					}
					if ($key == 'contacts') {
						$resutls_set['contacts']= DB::select(DB::raw("
									( SELECT
										id AS primaryKey,Cont_EntityID  AS entity ,Cont_EntityType  AS type, @table := 'contacts' AS tabal
										FROM contacts
										WHERE MATCH(Cont_Contact)
										AGAINST('$string' IN BOOLEAN MODE)
									)"));
					}
					if ($key == 'students') {
						# code...
					}
			}
		}

		$this->fetch($resutls_set);
	}


	public function limit($array,$postion)
	{
		// reads an array from a position
		// return all values up to the limit stop
		// calls fetch fn to to fect the details of the array from database
	}


	public function fetch($array)
	{
		// checks for the values
		$found = array();
		$results =  array();
			
			if (!empty($array)) {
				foreach ($array as $k => $val) {
					foreach ($val as $key => $value) {
						
						if ($value->tabal == 'departments') {

							if ( strtolower($value->entity) == strtolower($this->keywords) ) {
								$sch = Department::find($value->primaryKey)->toArray();
								$found['department'][] = $sch;
							}
							$sch = Department::find($value->primaryKey)->toArray();
							$results['department'][] = $sch;

						}else if($value->tabal == 'persons'){

							if ($value->entity == 'Staff') {



									$person = Person::with('staffs')->where('id','=',$value->primaryKey)->first()->toArray();


									if (strtolower($value->entity) == strtolower($this->keywords)) {

									$person = Person::with('staffs')->where('id','=',$value->primaryKey)->get()->toArray()[0];
									$found['person'][] = $person;
									}
									// $person = Person::find($value->primaryKey)->toArray();
									$person = Person::with('staffs')->where('id','=',$value->primaryKey)->get()->toArray()[0];
									$results['person'][] = $person;


								// $person = Person::with('staffs')->where('id','=',$value->primaryKey)->first()->toArray();

								

								// 	if (strtolower($value->entity) == strtolower($this->keywords)) {

								// 	$person = Person::with('staffs')->where('id','=',$value->primaryKey)->get();
								// 	$found['person'][] = $person;

								// 	}

								// 	$person = Person::with('staffs')->where('id','=',$value->primaryKey)->get()->toArray()[0];
								// 	// $person = Person::find($value->primaryKey)->toArray();
								// 	// dd($person);

								// 	$results['person'][] = $person;



							}
							if ($value->entity == 'Student') {

									

								$person = Person::with('students')->where('id','=',$value->primaryKey)->first()->toArray();


								

									if (strtolower($value->entity) == strtolower($this->keywords)) {

									$person = Person::with('students')->where('id','=',$value->primaryKey)->get()->toArray()[0];
									$found['person'][] = $person;
									}
									// $person = Person::find($value->primaryKey)->toArray();
									$person = Person::with('students')->where('id','=',$value->primaryKey)->get()->toArray()[0];
									$results['person'][] = $person;

								

								// 	if (	count($person ) > 1) {

								// 	if ($person['students'] != null) {

								// 	if (strtolower($value->entity) == strtolower($this->keywords)) {

								// 	$person = Person::find($value->primaryKey)->toArray();
								// 	$found['person'][] = $person;
								// 	}
								// 	$person = Person::find($value->primaryKey)->toArray();
								// 	$results['person'][] = $person;
								// }	
								// 	}else{


								// if ($person['students'] != null) {
								// 	if (strtolower($value->entity) == strtolower($this->keywords)) {
								// 	$person = Person::find($value->primaryKey)->toArray();
								// 	$found['person'][] = $person;
								// 	}
								// 	$person = Person::find($value->primaryKey)->toArray();
								// 	$results['person'][] = $person;										
								// 	}

								// }							

							}

						}else if($value->tabal == 'courses'){

							
							if (strtolower($value->entity) == strtolower($this->keywords)) {
								$courses = Course::find($value->primaryKey)->toArray();
								$found['course'][] = $courses;
							}
							$courses = Course::find($value->primaryKey)->toArray();
							$results['course'][] = $courses;

						}else if($value->tabal == 'contacts'){

							$contacts = Contact::with('persons')->where('id','=',$value->primaryKey)->first()->toArray();

							if (strtolower($value->entity) == strtolower($this->keywords)) {
								$contact = $contacts;
								$found['contact'][] = $contact;
							}
							$contact = Contact::find($value->primaryKey)->toArray();
							$results['contact'][] = $contacts;


						}		
					}
				

				}
				//fn replace array 
				if (!empty($found)) {
					$results = $found;
					// dd($found);
				}

			}

			$this->results($results);
		}

	public function results($searchs)
	{

		$this->layout->content = View::make('admin.search')->with('data',$searchs);

	}



}