<?php

class PagesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /pages
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /pages/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /pages
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /pages/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /pages/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function options($id)
	{
		//
	}
	public function postFetch()
	{
		$input=Input::all();
		$student = Student::where('StudentID','=',$input['studentID'])->first();
		
		// $this->layout->content = View::make('admin.students.show');
		if ( $student ) {
			$student = $student->toArray();
			return Redirect::to('students/'.$student['id']);
		}else{
			dd($student);
		}
		//
	}
	public function postFind()
	{
		$input = Input::all();
		//find the person
		// find the persons course in students
		$student =  array();
		if (empty($input['pers_mname'])) {
		$student = Person::with('students')->whereRaw('pers_type = ? AND pers_fname = ? AND pers_lname = ? AND pers_DOB = ? ',['Student',$input['pers_fname'],$input['pers_lname'],$input['pers_DOB']])->get();
		}else{
		$student =  Person::with('students')->whereRaw('pers_type = ? AND pers_fname = ? AND pers_mname = ? AND pers_lname = ? AND pers_DOB = ? ',['Student',$input['pers_fname'],$input['pers_mname'],$input['pers_lname'],$input['pers_DOB']])->get();
		}
		$check = $student->toArray(); #loop hole for error
		$students = array();
		$found = array();
		$result = array();
		if (!empty($check)) {
			foreach ($check as $key => $value) {
				if (isset($value['students'])) {
					if ($input['course_id'] == $value['students']['course_id']) {
						$found = $value;
					}else{
						$result = $value;
					}
				}
			}
		}

		if (!empty($found)) {
				return Redirect::to('students/'.$found['students']['id']);
		}else{
			Session::flash('message', "Student Not Found");
			return Redirect::back();
		}
	}

}