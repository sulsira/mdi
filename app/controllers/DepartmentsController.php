<?php

class DepartmentsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /departments
	 *
	 * @return Response
	 */
	public function index()
	{
		$departments = Department::with('courses')->where('deleted','=',0)->get();
		
		$this->layout->content = View::make('admin.department.index')->with('departments',$departments);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /departments/create
	 *
	 * @return Response
	 */
	public function create()
	{

		$this->layout->content = View::make('admin.department.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /departments
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		array_pop($input);
		$validation = Validator::make($input, ['name'=>'required']);
		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput();
		}
		$dept = Department::create($input);
		Session::flash('success', $input['name']);
		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 * GET /departments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /departments/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /departments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /departments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}