<?php

class UsersController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index()
	{

		$users = User::all();
		$users = $users ? $users->toArray() : []; 

		// $this->layout->content = View::make('admin.Users.index')->with('users',$users);
		// $users = User::where('deleted','!=',1)->get()->toArray();
		// // $this->data['users'] = $users;
		// dd(($users));
		$this->layout->content = View::make('admin.Users.index', compact('users'));

		// $this->layout->content = View::make('admin.Users.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$rules = [
				'username'=> 'max:200',
				'email'=> 'required|unique:users',
				'password'=> 'required|max:200',
				'Previleges'=> 'max:200'
			];
		$validation = Validator::make($input, $rules);
		if($validation->passes()){

			$user = User::create(array(
				'email'=> $input['email'],
				'password'=>Hash::make($input['password']),
				'deleted'=> 0)
			);
			if ($user) {
				// UserRole::create(array(
				// 	'user_id' => $user->id,
				// 	'fullname' => $input['username'],
				// 	'privileges' => $input['Previleges'],
				// 	'userGroup' => $input['accountType'],
				// 	'security_level' => $input['security_level'],
				// 	'url'=> '/'.$input['accountType']
				// ));
				Flash::overlay('Your have added a user');
				return Redirect::back();
			}




		}else{



			$errors = $validation->messages();
			return Redirect::back()->withErrors($errors)->withInput();							
		}
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$user = User::destroy($id);
		return Redirect::to('users');
	}

}