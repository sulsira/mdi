<?php

class WorkController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /work
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /work/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /work
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		if(Request::ajax()){

			if ($input['type'] == 'work') {
				
				$from = date("Y-m-d H:i:s", strtotime($input['from']));
				$to = date("Y-m-d H:i:s", strtotime($input['to']));
				
				return Response::json(  $input   );
			
			}
		}
	}

	/**
	 * Display the specified resource.
	 * GET /work/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /work/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /work/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /work/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}