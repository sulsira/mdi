<?php

class CertificatesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /certificates
	 *
	 * @return Response 
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /certificates/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /certificates
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validator = Validator::make($input, ['doc'=>'mimes:doc,docx,pdf|max:2000']);
		if($validator->fails()){
			return Redirect::back()->withErrors($validator->messages());
		}else{

					if ($input['doc']) {

							$shortened = base_convert(rand(10000,99999), 10, 36);
							$not = 'media'.'/'.'students';
							$foldername = '/'.$not.'/';
							
							$path = base_path().$foldername ;
							$fileexten = Input::file('doc')->getClientOriginalExtension();
							$filename = Input::file('doc')->getClientOriginalName();
							
							$rename = $shortened.'_document_.'.$filename;
							$filetyp = Input::file('doc')->getMimeType();
							$fileexten = Input::file('doc')->getClientOriginalExtension();
							$uploaded = false;

							if ( File::exists($path)  && ( File::exists( $path.'/'.'document'.'/' ) ) )  {

								Input::file('doc')->move($path.'/'.'document'.'/'  , $rename);

								// store in the database
									$document = new Certificate;
									$document->folder = $foldername;
									$document->url = $path.'document'.'/'.$rename ;
									$document->cert_academicID = $input['id'] ;
									$document->save();
								
								# code...
							}else{
								File::exists($path) ?: mkdir($path, 0755, true);
								// mkdir($path.'doc'.'/', 0755, true);
								Input::file('doc')->move($path.'/'.'document'.'/' , $rename);

								// store in the database
									$document = new Certificate;
									$document->folder = $foldername;
									$document->url = $path.'document'.'/'.$rename ;
									$document->cert_academicID = $input['id'] ;
									$document->save();
							}


					} #end of photo


				if (isset($input['done'])) {

					return Redirect::to("students/create");

					
				}elseif(isset($input['upload'])){

					return Redirect::back();


				}


		}
	}

	/**
	 * Display the specified resource.
	 * GET /certificates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /certificates/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /certificates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /certificates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}