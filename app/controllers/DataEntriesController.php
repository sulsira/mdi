<?php

class DataEntriesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /dataentries
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->layout->content = View::make('admin.DataEntries.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /dataentries/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /dataentries
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		if(Request::ajax()){
			if ($input['type'] == 'aschool') {
				$from = date("Y-m-d H:i:s", strtotime($input['from']));
				$to = date("Y-m-d H:i:s", strtotime($input['to']));
				$academic =  new Academic;
				$academic->aca_schoolname= $input['SchoolName'];
				$academic->aca_studentID   = $input['studentID'];
				$academic->aca_datefrom = $from;
				$academic->aca_dateto  = $to;
				$academic->save();
				return Response::json(  $academic   );
			
			}elseif($input['type'] == 'grades'){

				$grades = [];



				foreach ($input as $key => $value) {

					if(!empty($value['sub']) && !empty($value['grd'])){

						$grade = new Grade;
						$grade->ace_recordID = $input['id'];
						$grade->subject = $value['sub'] ;
						$grade->grade = $value['grd'];
						$grade->save();
						$grades = $grade;

					}

				}
				return Response::json($grades);





			}elseif($input['type'] == 'fileUpload'){

				return Response::json( Input::file('doc') );

			}else{

				return Response::json($input);

			}
			
		} #end of ajax
	}

	/**
	 * Display the specified resource.
	 * GET /dataentries/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$student = array();
		// $id = (Request::segment(4)) ?: $stdID;
		$student = Student::with('persons.document','course')->where('id','=',$id);

		$student = ($student->get()->toArray())? $student->first()->toArray(): [];
		$this->layout->content = View::make('admin.DataEntries.show',compact('student'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /dataentries/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /dataentries/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /dataentries/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}