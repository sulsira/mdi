<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admin
	 *
	 * @return Response
	 */
	protected $layout = 'admin';
	protected $menu = array();
	protected $data = array();
	public function __construct(){

			$userDetails = Auth::user();
			$username = $userDetails->email;
			// if (empty($username)) {
			// 	$username = $userDetails->fullname || $userDetails->email;
			// 	// $username = 'username';
			// }
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/admin','priv'=>'veda','type'=>'single'],
			['name'=>'departments','visible'=> 1 , 'url'=>'/departments','priv'=>'veda','type'=>'single'],
			['name'=>'courses','visible'=> 1 , 'url'=>'/courses','priv'=>'veda','type'=>'single'],
			['name'=>'students','visible'=> 1 , 'url'=>'/students','priv'=>'veda','type'=>'single'],
			['name'=>'staff','visible'=> 1 , 'url'=>'/staffs','priv'=>'veda','type'=>'single'],
			['name'=>'reports','visible'=> 0 , 'url'=>'admin/reports','priv'=>'veda','type'=>'single'],
			['name'=>'files','visible'=> 0 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'Users','visible'=> 1 , 'url'=>'users','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 0 , 'url'=>'help','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 0 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'users','visible'=> 0 , 'url'=>'users','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> 1 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data = $this->menu;
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});				
	}
	public function index()
	{
		$this->layout->content = View::make('admin.index')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}