-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 26, 2014 at 09:47 PM
-- Server version: 5.5.28
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mdidb`
--
CREATE DATABASE IF NOT EXISTS `mdidb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mdidb`;

-- --------------------------------------------------------

--
-- Table structure for table `academics`
--

CREATE TABLE IF NOT EXISTS `academics` (
  `aca_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aca_studentID` int(11) NOT NULL DEFAULT '0',
  `aca_schoolname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aca_datefrom` date DEFAULT NULL,
  `aca_dateto` date DEFAULT NULL,
  `aca_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aca_grade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aca_schoolType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aca_uppersecondaryFrom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`aca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE IF NOT EXISTS `addresses` (
  `Addr_AddressID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Addr_EntityID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_EntityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_AddressStreet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_POBox` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_District` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_CareOf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Addr_AddressID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`Addr_AddressID`, `Addr_EntityID`, `Addr_EntityType`, `Addr_AddressStreet`, `Addr_POBox`, `Addr_Town`, `Addr_Region`, `Addr_District`, `Addr_Country`, `Addr_CareOf`, `Addr_Deleted`, `created_at`, `updated_at`) VALUES
(1, '15', 'Person', 'kololi', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:11:31', '2014-08-09 05:11:31'),
(2, '16', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:13:47', '2014-08-09 05:13:47'),
(3, '17', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:16:25', '2014-08-09 05:16:25'),
(4, '18', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:24:20', '2014-08-09 05:24:20'),
(5, '19', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:25:34', '2014-08-09 05:25:34'),
(6, '20', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:27:24', '2014-08-09 05:27:24'),
(7, '21', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:31:40', '2014-08-09 05:31:40'),
(8, '22', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:32:58', '2014-08-09 05:32:58'),
(9, '23', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:33:03', '2014-08-09 05:33:03'),
(10, '24', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:33:18', '2014-08-09 05:33:18'),
(11, '25', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:33:27', '2014-08-09 05:33:27'),
(12, '26', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:33:55', '2014-08-09 05:33:55'),
(13, '27', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:41:20', '2014-08-09 05:41:20'),
(14, '28', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:42:28', '2014-08-09 05:42:28'),
(15, '29', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 05:45:41', '2014-08-09 05:45:41'),
(16, '30', 'Person', '', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:07:25', '2014-08-09 06:07:25'),
(17, '31', 'Person', '', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:07:56', '2014-08-09 06:07:56'),
(18, '32', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:11:21', '2014-08-09 06:11:21'),
(19, '33', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:13:59', '2014-08-09 06:13:59'),
(20, '34', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:22:40', '2014-08-09 06:22:40'),
(21, '35', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:24:23', '2014-08-09 06:24:23'),
(22, '36', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:25:58', '2014-08-09 06:25:58'),
(23, '37', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:27:06', '2014-08-09 06:27:06'),
(24, '38', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 06:29:09', '2014-08-09 06:29:09'),
(25, '39', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-09 21:30:06', '2014-08-09 21:30:06'),
(26, '40', 'Person', 'street', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-10 02:35:14', '2014-08-10 02:35:14'),
(27, '41', 'Person', 'i dontkno', NULL, 'kololi', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-10 02:40:12', '2014-08-10 02:40:12'),
(28, '42', 'Person', 'asdfafaa', NULL, 'asdfasdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-10 02:41:06', '2014-08-10 02:41:06'),
(29, '43', 'Person', 'city', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-10 02:45:59', '2014-08-10 02:45:59'),
(30, '44', 'Person', 'dr. ceessat', NULL, 'kololi', NULL, 'Serre Kunda East', NULL, NULL, 0, '2014-08-11 14:14:07', '2014-08-11 14:14:07'),
(31, '45', 'Person', 'str. sslfjs', NULL, 'kongue', NULL, 'Bakau', NULL, NULL, 0, '2014-08-11 14:19:05', '2014-08-11 14:19:05'),
(32, '46', 'Person', 'DR. CESSAY', NULL, 'KOLOLI', NULL, 'Serre Kunda Central', NULL, NULL, 0, '2014-08-11 14:23:08', '2014-08-11 14:23:08'),
(33, '47', 'Person', 'street', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-15 05:54:50', '2014-08-15 05:54:50'),
(34, '48', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-15 06:01:04', '2014-08-15 06:01:04'),
(35, '49', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-17 22:06:28', '2014-08-17 22:06:28'),
(36, '50', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-17 22:06:50', '2014-08-17 22:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE IF NOT EXISTS `certificates` (
  `cert_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cert_studentID` int(11) NOT NULL DEFAULT '0',
  `cert_academicID` int(11) NOT NULL DEFAULT '0',
  `cert_schoolName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cert_from` date DEFAULT NULL,
  `cert_to` date DEFAULT NULL,
  `cert_subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cert_grades` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `start_term` date DEFAULT NULL,
  `end_term` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Cont_ContactInfoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_EntityID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_EntityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_Contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_ContactType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `Cont_ContactInfoID`, `Cont_EntityID`, `Cont_EntityType`, `Cont_Contact`, `Cont_ContactType`, `Cont_Deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, '28', 'Person', 'asdfadsfa', 'phones', 0, '2014-08-09 05:42:28', '2014-08-09 05:42:28'),
(2, NULL, '28', 'Person', 'asdfasdfas', 'email', 0, '2014-08-09 05:42:28', '2014-08-09 05:42:28'),
(3, NULL, '28', 'Person', 'adfadfafd', 'telephone', 0, '2014-08-09 05:42:28', '2014-08-09 05:42:28'),
(4, NULL, '30', 'Person', '7052217,2545', 'phones', 0, '2014-08-09 06:07:25', '2014-08-09 06:07:25'),
(5, NULL, '31', 'Person', '7052217,2545', 'phones', 0, '2014-08-09 06:07:56', '2014-08-09 06:07:56'),
(6, NULL, '40', 'Person', '7052217', 'phones', 0, '2014-08-10 02:35:14', '2014-08-10 02:35:14'),
(7, NULL, '40', 'Person', 'something@soemting,', 'email', 0, '2014-08-10 02:35:14', '2014-08-10 02:35:14'),
(8, NULL, '40', 'Person', '65464', 'telephone', 0, '2014-08-10 02:35:14', '2014-08-10 02:35:14'),
(9, NULL, '41', 'Person', '7052217', 'phones', 0, '2014-08-10 02:40:13', '2014-08-10 02:40:13'),
(10, NULL, '41', 'Person', 'suls@is.com', 'email', 0, '2014-08-10 02:40:13', '2014-08-10 02:40:13'),
(11, NULL, '41', 'Person', '705217', 'telephone', 0, '2014-08-10 02:40:13', '2014-08-10 02:40:13'),
(12, NULL, '43', 'Person', '7052217', 'phones', 0, '2014-08-10 02:45:59', '2014-08-10 02:45:59'),
(13, NULL, '44', 'Person', '7052217', 'phones', 0, '2014-08-11 14:14:07', '2014-08-11 14:14:07'),
(14, NULL, '44', 'Person', 'sulsira@hotmail.com', 'email', 0, '2014-08-11 14:14:07', '2014-08-11 14:14:07'),
(15, NULL, '44', 'Person', '4461282', 'telephone', 0, '2014-08-11 14:14:07', '2014-08-11 14:14:07'),
(16, NULL, '45', 'Person', '542814641', 'phones', 0, '2014-08-11 14:19:05', '2014-08-11 14:19:05'),
(17, NULL, '45', 'Person', 'ogy@someth.com', 'email', 0, '2014-08-11 14:19:05', '2014-08-11 14:19:05'),
(18, NULL, '45', 'Person', '7254564654', 'telephone', 0, '2014-08-11 14:19:05', '2014-08-11 14:19:05'),
(19, NULL, '46', 'Person', '7779928', 'phones', 0, '2014-08-11 14:23:08', '2014-08-11 14:23:08'),
(20, NULL, '46', 'Person', 'SOMETHING@SOMETHING.COM', 'email', 0, '2014-08-11 14:23:08', '2014-08-11 14:23:08'),
(21, NULL, '46', 'Person', '7846456', 'telephone', 0, '2014-08-11 14:23:08', '2014-08-11 14:23:08'),
(22, NULL, '47', 'Person', '7052217', 'phones', 0, '2014-08-15 05:54:50', '2014-08-15 05:54:50'),
(23, NULL, '47', 'Person', 'sulsi@hotmai.com', 'email', 0, '2014-08-15 05:54:50', '2014-08-15 05:54:50'),
(24, NULL, '47', 'Person', '55340352453', 'telephone', 0, '2014-08-15 05:54:50', '2014-08-15 05:54:50'),
(25, NULL, '48', 'Person', '705211654', 'phones', 0, '2014-08-15 06:01:04', '2014-08-15 06:01:04'),
(26, NULL, '49', 'Person', '7052217', 'phones', 0, '2014-08-17 22:06:28', '2014-08-17 22:06:28'),
(27, NULL, '49', 'Person', 'email@email.com', 'email', 0, '2014-08-17 22:06:28', '2014-08-17 22:06:28'),
(28, NULL, '49', 'Person', '5645', 'telephone', 0, '2014-08-17 22:06:28', '2014-08-17 22:06:28'),
(29, NULL, '50', 'Person', '7052217', 'phones', 0, '2014-08-17 22:06:50', '2014-08-17 22:06:50'),
(30, NULL, '50', 'Person', 'email@email.com', 'email', 0, '2014-08-17 22:06:50', '2014-08-17 22:06:50'),
(31, NULL, '50', 'Person', '5645413684', 'telephone', 0, '2014-08-17 22:06:50', '2014-08-17 22:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `depart_id` int(11) NOT NULL DEFAULT '0',
  `staffID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_qualification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qualification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visibility` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `depart_id`, `staffID`, `name`, `fee`, `entry_qualification`, `qualification`, `duration`, `visibility`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'asdfadsfasfa', '', '', '', '', 0, 0, '2014-08-09 20:50:07', '2014-08-09 20:50:07'),
(2, 1, 2, 'asdfasdfaf', '', '2014-08-12', '', '', 0, 0, '2014-08-09 20:52:02', '2014-08-09 20:52:02'),
(3, 1, 2, 'something', '', '', '', '', 0, 0, '2014-08-09 20:53:39', '2014-08-09 20:53:39'),
(4, 1, 3, 'dsfadfadfaf', '20000', '2014-08-26', '', '', 0, 0, '2014-08-09 20:56:27', '2014-08-09 20:56:27'),
(5, 1, 4, 'asdfadsfadsfa', '', '', '', '', 0, 0, '2014-08-09 21:00:10', '2014-08-09 21:00:10'),
(6, 2, 9, 'information systems', '36000', '2014-08-08', 'Bachelors in IS', '12', 0, 0, '2014-08-11 14:20:48', '2014-08-11 14:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `courses_levels`
--

CREATE TABLE IF NOT EXISTS `courses_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_id` int(11) NOT NULL DEFAULT '0',
  `personName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` tinyint(1) NOT NULL DEFAULT '0',
  `lickId` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `person_id`, `personName`, `link`, `lickId`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'management', 0, 'mamadou jallow', 0, 0, 0, '2014-08-09 18:22:32', '2014-08-09 18:22:32'),
(2, 'computer science', 0, 'ebrima touray', 0, 0, 0, '2014-08-09 18:22:49', '2014-08-09 18:22:49'),
(3, 'physics', 0, 'demba bar', 0, 0, 0, '2014-08-09 18:23:00', '2014-08-09 18:23:00');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `doc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doc_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_entityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_entityID` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `doc_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_fullpath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_foldername` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_filetype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_userID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employments`
--

CREATE TABLE IF NOT EXISTS `employments` (
  `emp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `emp_studentID` int(11) NOT NULL DEFAULT '0',
  `emp_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_employer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_positionHeld` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_duties` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_08_06_004045_create_users_table', 1),
('2014_08_06_010536_create_usersRoles_table', 1),
('2014_08_06_011114_create_departments_table', 1),
('2014_08_06_011425_create_courses_table', 1),
('2014_08_06_011947_create_courses_levels_table', 1),
('2014_08_06_012553_create_classes_table', 1),
('2014_08_06_015724_create_persons_table', 1),
('2014_08_06_142135_create_students_table', 2),
('2014_08_06_143749_create_staff_table', 3),
('2014_08_06_144834_create_addresses_table', 4),
('2014_08_06_145319_create_contacts_table', 4),
('2014_08_06_145649_create_variables_table', 5),
('2014_08_08_143503_add_personName_to_departments_table', 6),
('2014_08_09_183249_add_staffID_to_courses_table', 7),
('2014_08_16_125752_add_studentID_students_table', 8),
('2014_08_21_013020_add_values_to_students_table', 9),
('2014_08_21_014131_create_Documents_table', 10),
('2014_08_21_020439_create_academics_table', 11),
('2014_08_21_034301_create_certificates_table', 12),
('2014_08_21_035246_create_employments_table', 13),
('2014_08_21_040425_create_payments_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `pay_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pay_studentID` int(11) NOT NULL DEFAULT '0',
  `pay_courseID` int(11) NOT NULL DEFAULT '0',
  `pay_payer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_totalAssesment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_initialPayment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_amountPaid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_orNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_receivedBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pers_indentifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_givenID` int(11) NOT NULL DEFAULT '0',
  `pers_fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_mname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_DOB` date DEFAULT NULL,
  `pers_gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_ethnicity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_standing` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_NIN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pers_fname` (`pers_fname`),
  KEY `pers_fname_2` (`pers_fname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `pers_indentifier`, `pers_givenID`, `pers_fname`, `pers_mname`, `pers_lname`, `pers_type`, `pers_DOB`, `pers_gender`, `pers_nationality`, `pers_ethnicity`, `pers_standing`, `pers_NIN`, `pers_deleted`, `created_at`, `updated_at`) VALUES
(1, 'null', 0, 'mamadou ', NULL, 'jallow', 'Staff', '2014-08-26', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:26:27', '2014-08-09 04:26:27'),
(2, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:37:40', '2014-08-09 04:37:40'),
(3, 'null', 0, 'maadou', NULL, 'jalow', 'Student', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:41:00', '2014-08-09 04:41:00'),
(4, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:41:17', '2014-08-09 04:41:17'),
(5, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:45:41', '2014-08-09 04:45:41'),
(6, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:46:31', '2014-08-09 04:46:31'),
(7, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:46:57', '2014-08-09 04:46:57'),
(8, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:54:28', '2014-08-09 04:54:28'),
(9, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:54:51', '2014-08-09 04:54:51'),
(10, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 04:59:38', '2014-08-09 04:59:38'),
(11, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:00:04', '2014-08-09 05:00:04'),
(12, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:01:05', '2014-08-09 05:01:05'),
(13, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:03:52', '2014-08-09 05:03:52'),
(14, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:06:31', '2014-08-09 05:06:31'),
(15, 'null', 0, 'mamadou', NULL, 'jallow', 'Staff', '2014-08-31', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:11:31', '2014-08-09 05:11:31'),
(16, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:13:47', '2014-08-09 05:13:47'),
(17, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:16:25', '2014-08-09 05:16:25'),
(18, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:24:20', '2014-08-09 05:24:20'),
(19, 'null', 0, 'maadou', NULL, 'jalow', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:25:34', '2014-08-09 05:25:34'),
(20, 'null', 0, 'alphi', NULL, 'jallow', 'Staff', '2015-01-19', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:27:24', '2014-08-09 05:27:24'),
(21, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:31:40', '2014-08-09 05:31:40'),
(22, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:32:58', '2014-08-09 05:32:58'),
(23, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:33:03', '2014-08-09 05:33:03'),
(24, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:33:18', '2014-08-09 05:33:18'),
(25, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:33:27', '2014-08-09 05:33:27'),
(26, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:33:55', '2014-08-09 05:33:55'),
(27, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:41:20', '2014-08-09 05:41:20'),
(28, 'null', 0, 'asdfafa', NULL, 'asdfasdfasfa', 'Staff', '2014-08-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:42:28', '2014-08-09 05:42:28'),
(29, 'null', 0, 'dafdfa', NULL, 'adfafdaf', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 05:45:41', '2014-08-09 05:45:41'),
(30, 'null', 0, 'adsfadfa', NULL, 'sdfadsfafa', 'Staff', '2014-08-14', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:07:24', '2014-08-09 06:07:24'),
(31, 'null', 0, 'adsfadfa', NULL, 'sdfadsfafa', 'Staff', '2014-08-14', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:07:56', '2014-08-09 06:07:56'),
(32, 'null', 0, 'mamadou', NULL, 'jallow', 'Staff', '2012-10-15', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:11:21', '2014-08-09 06:11:21'),
(33, 'null', 0, 'mamadou', NULL, 'jallow', 'Staff', '2012-10-15', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:13:59', '2014-08-09 06:13:59'),
(34, 'null', 0, 'asfasdfafda', NULL, 'asdfadf', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:22:40', '2014-08-09 06:22:40'),
(35, 'null', 0, 'afdfadsfa', NULL, 'adsfadfa', 'Staff', '1922-02-08', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:24:23', '2014-08-09 06:24:23'),
(36, 'null', 0, 'dfasdf', NULL, 'adsfa', 'Staff', '2014-08-21', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:25:58', '2014-08-09 06:25:58'),
(37, 'null', 0, 'dsfa', NULL, 'asdasdadas', 'Staff', '2014-09-03', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:27:06', '2014-08-09 06:27:06'),
(38, 'null', 0, 'dsfadsfsa', 'shs ', 'sdfsfasdfa', 'Staff', '2014-08-07', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-09 06:29:09', '2014-08-09 06:29:09'),
(39, 'null', 0, 'mamadou', NULL, 'jallow', 'Staff', '2014-08-06', 'male', 'Antigua and Barbuda', 'Mandinka', '0', '0', 0, '2014-08-09 21:30:06', '2014-08-09 21:30:06'),
(40, 'null', 0, 'mamadou', NULL, 'jallo', 'Staff', '2014-08-20', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-10 02:35:14', '2014-08-10 02:35:14'),
(41, 'null', 0, 'mamadou', NULL, 'jallow', 'Staff', '2014-08-14', 'male', 'Antigua and Barbuda', 'Wollof', '0', '0', 0, '2014-08-10 02:40:12', '2014-08-10 02:40:12'),
(42, 'null', 0, 'sdfadsfa', NULL, 'adsfa', 'Staff', '2014-08-19', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-10 02:41:06', '2014-08-10 02:41:06'),
(43, 'null', 0, 'mamadou', NULL, 'jallo', 'Student', '2014-08-21', 'male', 'Anguilla', 'Fula', '0', '0', 0, '2014-08-10 02:45:59', '2014-08-10 02:45:59'),
(44, 'null', 0, 'mamadou ', 'shs', 'jallow', 'Student', '1988-01-28', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-11 14:14:07', '2014-08-11 14:14:07'),
(45, 'null', 0, 'antony', NULL, 'ogysomething', 'Staff', '1960-07-28', 'male', 'Nigeria', 'Other', '0', '0', 0, '2014-08-11 14:19:05', '2014-08-11 14:19:05'),
(46, 'null', 0, 'EBRIMA', NULL, 'FANTA', 'Student', '1986-08-15', 'male', 'Gambia, The', 'Wollof', '0', '0', 0, '2014-08-11 14:23:08', '2014-08-11 14:23:08'),
(47, 'null', 0, 'mamadou ', NULL, 'fofana', 'Student', '2014-08-27', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-15 05:54:50', '2014-08-15 05:54:50'),
(48, 'null', 0, 'kolola', NULL, 'jimbo', 'Student', '2014-08-31', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-15 06:01:04', '2014-08-15 06:01:04'),
(49, 'null', 0, 'ebrima', NULL, 'faty', 'Student', '2014-08-17', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-17 22:06:28', '2014-08-17 22:06:28'),
(50, 'null', 0, 'ebrima', NULL, 'faty', 'Student', '2014-08-17', 'male', 'Gambia, The', 'Fula', '0', '0', 0, '2014-08-17 22:06:50', '2014-08-17 22:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Staff_PersonID` int(11) NOT NULL DEFAULT '0',
  `Staff_Rank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Staff_HighestQualification` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Staff_Role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Staff_FieldOfTeaching` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Staff_MainLevelTeaching` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Staff_HireDate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Staff_EmploymentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dept_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `Staff_PersonID`, `Staff_Rank`, `Staff_HighestQualification`, `Staff_Role`, `Staff_FieldOfTeaching`, `Staff_MainLevelTeaching`, `Staff_HireDate`, `Staff_EmploymentType`, `dept_id`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 31, 'Principal Tutor', 'Masters', 'Academic', '15', 'Post Graduate Certificate', NULL, NULL, 1, 0, '2014-08-09 06:07:56', '2014-08-09 06:07:56'),
(2, 32, 'Professor', 'Certificate', 'Academic', '1', 'Certificate', NULL, NULL, 1, 0, '2014-08-09 06:11:21', '2014-08-09 06:11:21'),
(3, 33, 'Professor', 'Certificate', 'Academic', '1', 'Certificate', NULL, NULL, 1, 0, '2014-08-09 06:13:59', '2014-08-09 06:13:59'),
(4, 34, 'Tutor', 'Doctorate (PhD)', 'Both', '12', 'Doctorate (PhD)', NULL, NULL, 2, 0, '2014-08-09 06:22:40', '2014-08-09 06:22:40'),
(5, 35, 'Professor', 'null', 'Academic', '0', 'null', NULL, NULL, 3, 0, '2014-08-09 06:24:23', '2014-08-09 06:24:23'),
(6, 36, 'Professor', 'null', 'Academic', '0', 'null', NULL, NULL, 2, 0, '2014-08-09 06:25:59', '2014-08-09 06:25:59'),
(7, 37, 'Tutor', 'Advanced Diploma', 'Both', '13', 'Doctorate (PhD)', NULL, NULL, 3, 0, '2014-08-09 06:27:06', '2014-08-09 06:27:06'),
(8, 38, 'Professor', 'null', 'Academic', '0', 'null', NULL, NULL, 3, 0, '2014-08-09 06:29:09', '2014-08-09 06:29:09'),
(9, 45, 'Lecturer', 'Masters', 'Academic', '2', 'Bachelor', NULL, NULL, 2, 0, '2014-08-11 14:19:05', '2014-08-11 14:19:05');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL DEFAULT '0',
  `admission_date` date DEFAULT NULL,
  `course_id` int(11) NOT NULL DEFAULT '0',
  `graduated` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `working` int(11) NOT NULL DEFAULT '0',
  `transferee` int(11) NOT NULL DEFAULT '0',
  `completion_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `studentID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `person_id`, `admission_date`, `course_id`, `graduated`, `status`, `working`, `transferee`, `completion_date`, `created_at`, `updated_at`, `studentID`) VALUES
(1, 43, '2011-08-19', 1, 0, NULL, 0, 0, NULL, '2014-08-09 21:30:06', '2014-08-09 21:30:06', 'code1'),
(2, 0, '2012-08-19', 0, 0, NULL, 0, 0, NULL, '2014-08-10 02:35:14', '2014-08-10 02:35:14', 'code0'),
(3, 3, '2013-08-19', 5, 0, NULL, 0, 0, NULL, '2014-08-10 02:45:59', '2014-08-10 02:45:59', 'code3'),
(4, 44, '2014-02-28', 3, 0, NULL, 0, 0, NULL, '2014-08-11 14:14:07', '2014-08-11 14:14:07', 'code4'),
(5, 46, '2014-08-25', 6, 0, NULL, 0, 0, NULL, '2014-08-11 14:23:08', '2014-08-11 14:23:08', 'code5'),
(6, 47, '2014-08-30', 6, 0, NULL, 0, 0, NULL, '2014-08-15 05:54:50', '2014-08-15 05:54:50', 'code6'),
(7, 48, '2014-08-24', 4, 0, NULL, 0, 0, NULL, '2014-08-15 06:01:04', '2014-08-15 06:01:04', 'code7'),
(8, 50, '2014-08-25', 6, 0, NULL, 0, 0, NULL, '2014-08-17 22:06:50', '2014-08-17 22:06:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hpwd` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` tinyint(1) NOT NULL DEFAULT '0',
  `lickId` int(11) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `privileges` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` tinyint(1) NOT NULL DEFAULT '0',
  `lickId` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE IF NOT EXISTS `variables` (
  `Vari_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Vari_VariableName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vari_Table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vari_Field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Vari_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=502 ;

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`Vari_ID`, `Vari_VariableName`, `Vari_Table`, `Vari_Field`, `created_at`, `updated_at`, `Deleted`) VALUES
(50, 'University', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(51, 'College', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(52, 'Vocational', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(53, 'Technical', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(54, 'Other', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(55, 'Public', 'LearningCenter', 'LeCe_Ownership', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(56, 'Private', 'LearningCenter', 'LeCe_Ownership', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(58, 'Full Time', 'Class', 'Clas_AttendanceStatus', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(59, 'Part Time', 'Class', 'Clas_AttendanceStatus', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(60, 'Male', 'Student', 'Stud_Sex', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(61, 'Female', 'Student', 'Stud_Sex', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(62, 'GABECE', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(63, 'WASSCE', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(64, 'O ', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(65, 'A', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(66, 'Pre-Entry', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(67, 'Bachelor', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(68, 'Government', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(69, 'Private', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(70, 'NGO', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(71, 'Other', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(76, 'Professor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(77, 'Assoc. Professor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(78, 'Senior Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(79, 'Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(80, 'Asst. Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(82, 'Chief Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(83, 'Principal Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(84, 'Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(85, 'Asst. Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(91, 'Admin/Support- Full Time', 'Staff', 'Staff_Role/Status', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(92, 'Admin/Support- Part Time', 'Staff', 'Staff_Role/Status', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(93, 'Learning Center', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(94, 'Student', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(95, 'Staff', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(96, 'Mobile', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(97, 'LAN Line', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(98, 'FAX', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(100, 'Gambia, The', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(101, 'Senegal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(102, 'Algeria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(103, 'Angola', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(104, 'Anguilla', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(105, 'Antigua and Barbuda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(106, 'Argentina', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(107, 'Armenia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(108, 'Aruba', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(109, 'Australia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(110, 'Austria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(111, 'Azerbaijan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(112, 'Bahamas', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(113, 'Bahrain', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(114, 'Bangladesh', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(115, 'Barbados', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(116, 'Belarus', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(117, 'Belgium', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(118, 'Belize', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(119, 'Benin', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(120, 'Bermuda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(121, 'Bhutan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(122, 'Bolivia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(123, 'Bosnia and Herzegovina', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(124, 'Botswana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(125, 'Brazil', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(126, 'British Virgin Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(127, 'Brunei Darussalam', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(128, 'Bulgaria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(129, 'Burkina Faso', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(130, 'Burundi', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(131, 'Cambodia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(132, 'Cameroon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(133, 'Canada', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(134, 'Cape Verde', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(135, 'Cayman Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(136, 'Central African Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(137, 'Chad', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(138, 'Chile', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(139, 'China', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(140, 'Colombia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(141, 'Comoros', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(142, 'Congo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(143, 'Cook Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(144, 'Costa Rica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(145, 'C', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(146, 'Croatia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(147, 'Cuba', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(148, 'Cyprus', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(149, 'Czech Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(150, 'Demo. Repub. of the Congo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(151, 'Denmark', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(152, 'Dominica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(153, 'Dominican Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(154, 'Ecuador', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(155, 'Egypt', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(156, 'El Salvador', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(157, 'Equatorial Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(158, 'Eritrea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(159, 'Estonia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(160, 'Ethiopia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(161, 'EUROPE not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(162, 'Fiji', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(163, 'Finland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(164, 'France', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(165, 'Gabon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(167, 'Georgia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(168, 'Germany', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(169, 'Ghana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(170, 'Gibraltar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(171, 'Greece', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(172, 'Grenada', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(173, 'Guatemala', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(174, 'Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(175, 'Guinea-Bissau', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(176, 'Guyana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(177, 'Haiti', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(178, 'Holy See', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(179, 'Honduras', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(180, 'Hong Kong (SAR)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(181, 'Hungary', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(182, 'Iceland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(183, 'India', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(184, 'Indonesia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(185, 'Iran (Islamic Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(186, 'Iraq', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(187, 'Ireland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(188, 'Israel', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(189, 'Italy', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(190, 'Jamaica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(191, 'Japan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(192, 'Jordan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(193, 'Kazakhstan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(194, 'Kenya', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(195, 'Kiribati', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(196, 'Korea (Democratic People', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(197, 'Korea (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(198, 'Kuwait', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(199, 'Kyrgyzstan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(200, 'Lao People', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(201, 'Latvia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(202, 'Lebanon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(203, 'Lesotho', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(204, 'Liberia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(205, 'Libyan Arab Jamahiriya', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(206, 'Liechtenstein', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(207, 'Lithuania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(208, 'Luxembourg', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(209, 'Macao (China)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(210, 'Madagascar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(211, 'Malawi', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(212, 'Malaysia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(213, 'Maldives', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(214, 'Malta', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(215, 'Marshall Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(216, 'Mauritania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(217, 'Mauritius', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(218, 'Mexico', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(219, 'Micronesia (Federal States of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(220, 'Moldova (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(221, 'Monaco', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(222, 'Mongolia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(223, 'Montserrat', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(224, 'Morocco', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(225, 'Mozambique', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(226, 'Myanmar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(227, 'Namibia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(228, 'Nauru', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(229, 'Nepal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(230, 'Netherlands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(231, 'Netherlands Antilles', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(232, 'New Zealand', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(233, 'Nicaragua', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(234, 'Niger', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(235, 'Nigeria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(236, 'Niue', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(237, 'NORTH AMERICA not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(238, 'Norway', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(239, 'OCEANIA not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(240, 'Oman', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(241, 'Pakistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(242, 'Palau (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(243, 'Palestinian Autonomous Territories', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(244, 'Panama', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(245, 'Papua New Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(246, 'Paraguay', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(247, 'Peru', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(248, 'Philippines', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(249, 'Poland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(250, 'Portugal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(251, 'Qatar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(252, 'Romania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(253, 'Russian Federation', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(254, 'Rwanda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(255, 'Saint Kitts and Nevis', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(256, 'Saint Vincent and the Grenadines', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(257, 'Samoa', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(258, 'San Marino', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(259, 'Sao Tome and Principe', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(260, 'Saudi Arabia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(262, 'Serbia and Montenegro', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(263, 'Seychelles', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(264, 'Sierra Leone', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(265, 'Singapore', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(266, 'Slovakia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(267, 'Slovenia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(268, 'Solomon Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(269, 'Somalia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(270, 'South Africa', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(271, 'Spain', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(272, 'Sri Lanka', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(273, 'Sudan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(274, 'Suriname', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(275, 'Swaziland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(276, 'Sweden', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(277, 'Switzerland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(278, 'Syrian Arab Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(279, 'Tajikistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(280, 'Thailand', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(281, 'Republic of Macedonia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(282, 'Timor-Leste', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(283, 'Togo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(284, 'Tokelau', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(285, 'Tonga', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(286, 'Trinidad and Tobago', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(287, 'Tunisia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(288, 'Turkey', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(289, 'Turkmenistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(290, 'Turks and Caicos Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(291, 'Tuvalu', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(292, 'Uganda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(293, 'Ukraine', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(294, 'United Arab Emirates', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(295, 'United Kingdom', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(296, 'United Republic of Tanzania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(297, 'United States of America', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(298, 'Uruguay', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(299, 'Uzbekistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(300, 'Vanuatu', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(301, 'Venezuela', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(302, 'Viet Nam', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(303, 'Yemen', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(304, 'Zambia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(305, 'Zimbabwe', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(306, 'Certificate', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(307, 'Ordinary Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(308, 'Higher Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(309, 'Active', 'Standing', 'Standing', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(310, 'Inactive', 'Standing', 'Standing', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(311, 'Certificate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(312, 'Ordinary Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(313, 'Bachelor', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(314, 'Bachelor', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(315, 'Post Graduate Certificate', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(316, 'Post Graduate Diploma', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(317, 'Masters', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(318, 'Doctorate', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(320, '6', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(321, '12', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(322, '18', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(323, '24', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(324, '36', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(325, '48', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(326, '84', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(327, 'Other', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(328, 'Advanced Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(329, 'Bachelor', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(330, 'Masters', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(331, 'Horticulture', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(332, 'Electrical Installation', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(333, 'Carpentry', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(334, 'Building Construction', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(335, 'Surveying', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(336, 'Food Processing', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(337, 'Hospitality', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(338, 'Architecture', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(339, 'Hairdressing', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(340, 'Animal Husbandry', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(341, 'Basic Literacy', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(343, 'Certificate', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(344, 'Diploma', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(345, 'Student', 'Person', 'Pers_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(346, 'Staff', 'Person', 'Pers_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(347, 'Motor Vehicle Systems', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(349, 'Plumbing and Gas Fitting', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(350, 'Cooking and Baking', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(351, 'Sewing/Craft Work and Tie Dye', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(352, 'Home Economics', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(353, 'House Keeping', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(354, 'Refrigeration and Air Conditioning', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(355, 'Painting', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(356, 'Graphic Design', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(357, 'Draftsmanship', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(358, 'Secretarial', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(359, 'Computer Repair', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(360, 'Fashion Design', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(361, 'Mobile Repair', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(362, '1', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(363, '2', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(364, '3', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(365, '4', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(366, 'Basic Programmes', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(367, 'Teacher training and education science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(368, 'Education Science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(369, 'Arts', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(370, 'Humanities', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(371, 'Social and Behavioural Science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(372, 'Journalism and Information', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(373, 'Business and Administration', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(374, 'Law', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(375, 'Life Sciences', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(376, 'Physical Sciences', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(377, 'Mathematics and Statistics', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(378, 'Computing', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(379, 'Engineering and Engineering Trades', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(380, 'Manufacturing and Processing', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(381, 'Architecture and Building', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(382, 'Agriculture, Forestry and Fishery', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(383, 'Veterinary', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(384, 'Health', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(385, 'Social Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(386, 'Personal Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(387, 'Transport Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(388, 'Environmental Protection', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(389, 'Security Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(390, 'Unspecified', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(391, 'General Programmes', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(392, 'Education', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(393, 'Humanities and Arts', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(394, 'Social Sciences', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(395, 'Physical and Natural Science', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(396, 'Engineering, Manufacturing and Construction', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(397, 'Agriculture', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(398, 'Public and Medical Health Science', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(399, 'Tourism/Hospitality/Services', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(400, 'Not Known or Unspecified', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(401, 'Permanent', 'Staff', 'Staff_EmploymentType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(402, 'Temporary', 'Staff', 'Staff_EmploymentType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(403, 'Masters', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(404, 'Doctorate (PhD)', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(405, 'Post Graduate Certificate', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(406, 'Administraive staff', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(407, 'Instructor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(408, 'J.S.S Certificate', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(409, 'Business', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(410, 'Law', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(412, 'Information and Communication Technology', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(413, 'Basic Certificate', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(414, 'Certificate', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(415, 'Diploma	Student', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(416, 'Bachelor', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(417, 'Masters	Student', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(418, 'Doctorate', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(419, 'Post Graduate Certificate', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(420, 'Post Graduate Diploma', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(421, 'Phone', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(422, 'Email', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(423, 'Academic', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(424, 'Administrative', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(425, 'Both', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(426, 'Post Graduate Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(430, 'Banjul South', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(431, 'Banjul Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(432, 'Banjul North', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(433, 'Serre Kunda East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(434, 'Serre Kunda West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(435, 'Serre Kunda Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(436, 'Bakau', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(437, 'Kombo North', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(438, 'Kombo South', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(439, 'Kombo Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(440, 'Kombo East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(441, 'Foni Brefet', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(442, 'Foni Bintang', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(443, 'Foni Kansalla', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(444, 'Foni Bondali', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(445, 'Foni Jarrol', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(446, 'Kiang West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(447, 'Kiang Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(448, 'Kiang East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(449, 'Jarra West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(450, 'Jarra Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(451, 'Jarra East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(452, 'Lower Nuimi', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(453, 'Upper Nuimi', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(454, 'Jokadu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(455, 'Lower Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(456, 'Central Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(457, 'Upper Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(458, 'Niani', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(459, 'Lower Saloum', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(460, 'Upper Saloum', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(461, 'Nianija', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(462, 'Sami', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(463, 'Niamina West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(464, 'Niamina East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(465, 'Niamina Dankunko', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(466, 'Janjanbureh', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(467, 'Fuladu East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(468, 'Kantora', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(469, 'Wuli', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(470, 'Sandu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(471, 'Fuladu West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(475, 'Web site', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(476, 'Fula', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(477, 'Jola', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(478, 'Mandinka', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(479, 'Manjago', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(480, 'Sarahule', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(481, 'Serere', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(482, 'Tukulor', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(483, 'Wollof', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(484, 'Other', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(485, 'Fax', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(486, 'N/A', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(487, 'Masters', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(488, 'Doctorate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(489, 'Post Graduate Certificate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(490, 'Post Graduate Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(491, 'Higher Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(492, 'Advanced Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(493, 'Higher Education', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(494, 'Tertiary', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(495, 'TVET', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(496, 'Internal', 'Users', 'User_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(497, 'External', 'Users', 'User_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(498, 'Basic Literacy', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(499, 'Public', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(500, 'Government', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(501, 'Both', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
